import React, { useState, useEffect } from "react";
import { Form } from "reactstrap";
import {
  FiSettings,
  FiLogOut,
  FiHome,
  FiBell,
  FiList,
  FiPlusCircle,
  FiUser,
  FiEdit3,
} from "react-icons/fi";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { resetReducerAuth } from "../../Store/authSlice";
import { resetReducerCategory } from "../../Store/categorySlice";
import { resetReducerNotification } from "../../Store/notificationSlice";
import { resetReducerOffer } from "../../Store/offerSlice";
import { resetReducerProduct } from "../../Store/productSlice";

function AccountPage() {
  const navigate = useNavigate();
  const dataUser = localStorage.getItem("user");
  const dataUserObj = JSON.parse(dataUser);
  const imgUser = dataUser ? JSON.parse(dataUser).image : null;
  const [title, setTitle] = useState(`${dataUserObj.name} | RangKas`);
  const dispatch = useDispatch();

  useEffect(() => {
    document.title = title;
  }, [title]);

  function logoutAccount() {
    dispatch(resetReducerAuth());
    dispatch(resetReducerCategory());
    dispatch(resetReducerNotification());
    dispatch(resetReducerOffer());
    dispatch(resetReducerProduct());
    localStorage.clear();
    navigate("/");
  }

  return (
    <div className="md:grid md:place-items-center overflow-y-hidden relative top-6 lg:top-28">
      <Form className="w-full md:w-[36rem] md:h-full px-6">
        <h1 className="text-xl lg:text-2xl font-semibold mb-8">Akun Saya</h1>

        <div className="mx-auto h-24 relative w-24 bg-bgColorSecond rounded-xl flex items-center justify-center">
          {imgUser ? (
            <img
              src={imgUser}
              alt="avatar"
              className="w-full h-full object-cover rounded-xl"
            />
          ) : (
            <FiUser className="text-4xl text-btnPrimary" />
          )}
        </div>

        <ul className="mt-8">
          <li>
            <Link
              to="/edit-profile"
              className="flex py-4 border-b border-[#E5E5E5]"
            >
              <FiEdit3 className="text-[#7126B5] text-xl" />
              <p className="pl-4 text-sm font-medium">Ubah Akun</p>
            </Link>
          </li>
          <li>
            <Link
              className="flex py-4 border-b border-[#E5E5E5]"
              to="/change-password"
            >
              <FiSettings className="text-[#7126B5] text-xl" />
              <p className="pl-4 text-sm font-medium">Pengaturan Akun</p>
            </Link>
          </li>
          <li>
            <div
              onClick={logoutAccount}
              className="flex py-4 border-b border-[#E5E5E5] cursor-pointer"
            >
              <FiLogOut className="text-[#7126B5] text-xl" />
              <p className="pl-4 text-sm font-medium">Keluar</p>
            </div>
          </li>
        </ul>
      </Form>
      <div className="w-full border-t border-[#E5E5E5] md:hidden fixed bottom-0">
        <ul className="flex justify-around py-2">
          <li>
            <NavLink
              to="/"
              style={({ isActive }) => {
                return {
                  color: isActive ? "#7126B5" : "#8A8A8A",
                  cursor: "pointer",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                };
              }}
            >
              <FiHome className="text-xl" />
              <p className="text-xs font-medium">Home</p>
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/notification"
              style={({ isActive }) => {
                return {
                  color: isActive ? "#7126B5" : "#8A8A8A",
                  cursor: "pointer",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                };
              }}
            >
              <FiBell className="text-xl" />
              <p className="text-xs font-medium">Notifikasi</p>
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/add-new-product"
              style={({ isActive }) => {
                return {
                  color: isActive ? "#7126B5" : "#8A8A8A",
                  cursor: "pointer",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                };
              }}
            >
              <FiPlusCircle className="text-xl" />
              <p className="text-xs font-medium">Jual</p>
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/dashboard-seller"
              style={({ isActive }) => {
                return {
                  color: isActive ? "#7126B5" : "#8A8A8A",
                  cursor: "pointer",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                };
              }}
            >
              <FiList className="text-xl" />
              <p className="text-xs font-medium">DaftarJual</p>
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/profile"
              style={({ isActive }) => {
                return {
                  color: isActive ? "#7126B5" : "#8A8A8A",
                  cursor: "pointer",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                };
              }}
            >
              <FiUser className="text-xl" />
              <p className="text-xs font-medium">Akun</p>
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default AccountPage;

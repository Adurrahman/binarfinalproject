import React, { useEffect, useState, useRef } from "react";
import { FormGroup, Label } from "reactstrap";
import { FiPlus, FiArrowLeft, FiX } from "react-icons/fi";
import { useDispatch, useSelector } from "react-redux";
import { getAllCategory } from "../../Store/categorySlice";
import {
  previewProduct,
  getProductByID,
  updateProduct,
  previewEditProduct,
  delPreviewEditProduct,
} from "../../Store/productSlice";
import { Link, useNavigate, useParams } from "react-router-dom";
import Loader from "../../Component/Loader/Loader";

function UpdateProdukPage() {
  let { idProduct } = useParams();

  let navigate = useNavigate();
  const [title, setTitle] = useState("Update Product | RangKas");

  useEffect(() => {
    document.title = title;
  }, [title]);

  const changeTitle = (event) => {
    setTitle(event.target.value);
  };

  const formResult = useRef(null);
  const localUser = localStorage.getItem("user");
  const userObj = JSON.parse(localUser);

  const editableProduct = useSelector((state) => state.product.editProduct);
  const checkLoading = useSelector((state) => state.product.loading);
  const status = useSelector((state) => state.product.status);
  const error = useSelector((state) => state.product.isError);
  const category = useSelector((state) => state.category.category);
  const dispatch = useDispatch();

  const [errSizeImg, setErrSizeimg] = useState("");
  const [isempty, setIsEmpty] = useState(false);

  const [image, setImage] = useState(
    editableProduct ? editableProduct.images : []
  );
  const [oldImage, setoldImage] = useState(
    editableProduct ? editableProduct.unchanged_url : []
  );
  const [newImage, setnewImage] = useState(
    editableProduct ? editableProduct.newImage : []
  );

  const [nama, setNama] = useState(
    editableProduct ? editableProduct.product_name : ""
  );
  const [kategori, setKategori] = useState(
    editableProduct ? editableProduct.category_id : ""
  );
  const [deskripsi, setDeskripsi] = useState(
    editableProduct ? editableProduct.description : ""
  );
  const [harga, setHarga] = useState(
    editableProduct ? editableProduct.price : ""
  );

  const setPrice = (e) => {
    setHarga(e.target.value.replace(/\D/g, ""));
  };

  useEffect(() => {
    dispatch(getAllCategory());
  }, [dispatch]);

  function hanldechangeUpload(e) {
    let uploaded = e.target.files[0];
    const objcImgFile = { id: image.length, url: uploaded };
    let urlimage = URL.createObjectURL(uploaded);
    const objcImgurl = { id: image.length, url: urlimage };

    if (uploaded.size > 500000) {
      setErrSizeimg("Image Cannot Size More Than 500kb");
    } else {
      setErrSizeimg("");
      setImage([...image, objcImgurl]);
      setnewImage([...newImage, objcImgFile]);
    }
  }

  useEffect(() => {
    if (status === "succeeded") {
      navigate("/dashboard-seller", { replace: true });
    }
  }, [status, navigate]);

  function removeImage(index) {
    const a = image.filter((element, key) => {
      if (key === index) {
        return element;
      }
    });

    setImage(
      image.filter((element) => {
        if (element.id != a[0].id) return element;
      })
    );

    setoldImage(
      oldImage.filter((element) => {
        if (element.id != a[0].id) return element;
      })
    );

    setnewImage(
      newImage.filter((element) => {
        if (element.id != a[0].id) return element;
      })
    );
  }
  function handleSubmit(e) {
    e.preventDefault();
    if (nama && harga && kategori && deskripsi && image.length > 0) {
      const data = new FormData(formResult.current);
      data.append("user_id", userObj.id);

      newImage
        ? newImage.forEach((index) => {
            data.append("images", index.url);
          })
        : data.append("images", "");

      oldImage
        ? oldImage.forEach((index) => {
            data.append("unchanged_url", index.url);
          })
        : data.append("unchanged_url", "");

      data.append("description", deskripsi.replace(/\r?\n/g, "\n"));
      data.append("product_uuid", editableProduct.product_uuid);
      data.append("product_id", editableProduct.product_id);

      dispatch(updateProduct(data));
    } else {
      setIsEmpty(true);
    }
  }

  function seePreview() {
    if (nama && harga && kategori && deskripsi && image.length > 0) {
      const data = {
        product_id: idProduct,
        product_uuid: editableProduct.product_uuid,
        product_name: nama,
        price: harga,
        category_id: kategori,
        description: deskripsi,
        user_id: editableProduct.user_id,
        images: image,
        newImage: newImage,
        unchanged_url: oldImage,
      };
      dispatch(delPreviewEditProduct());

      dispatch(previewEditProduct(data));

      navigate(`/edit-product/${idProduct}`, { replace: true });
    } else {
      setIsEmpty(true);
    }
  }

  return (
    <>
      {checkLoading ? <Loader /> : <></>}
      <div className="w-full overflow-y-hidden p-8 lg:relative lg:top-20 2xl:px-[28rem] lg:px-60">
        <div className="lg:flex">
          {/* Navbar */}
          {window.innerWidth < 1024 ? (
            <div className="flex items-center w-full lg:w-auto lg:hidden justify-between mb-5">
              <Link to="/dashboard-seller">
                <FiArrowLeft
                  className="text-2xl"
                  onClick={() => navigate(-1)}
                />
              </Link>
              <h1 className="font-medium">Lengkapi Info Akun</h1>
              <div className="w-5"></div>
            </div>
          ) : (
            <Link to="/dashboard-seller">
              <FiArrowLeft className="text-2xl" />
            </Link>
          )}
          {/* End Of Navbar */}

          {/* Form Untuk Nama Produk */}
          <form
            ref={formResult}
            className="mb-20 lg:w-full lg:pl-5"
            onSubmit={handleSubmit}
          >
            <FormGroup className="mb-8">
              <Label className="block mb-3">Nama Produk</Label>
              <input
                className="outline-none border-2 px-3 py-2 rounded-xl w-full"
                placeholder="Nama Produk"
                type="text"
                name="product_name"
                value={nama}
                onChange={(e) => {
                  setNama(e.target.value);
                }}
                required
              />
            </FormGroup>
            {/* End Of Form Untuk Nama Produk */}

            {/* Form Untuk Harga Produk */}
            <FormGroup className="mb-8">
              <Label className="block mb-3">Harga Produk</Label>
              <input
                className="outline-none border-2 px-3 py-2 rounded-xl w-full"
                placeholder="Rp 0,00"
                type="text"
                name="price"
                value={harga}
                onChange={setPrice}
                required
              />
            </FormGroup>
            {/* End Of Form Untuk Harga Produk */}

            {/* Form Untuk Kategori */}
            <FormGroup className="mb-8">
              <Label className="block mb-3">Kategori</Label>

              {/* kategori bakal ambil dari api dan dilooping disini */}
              <select
                name="category_id"
                className="w-full outline-none border-2 px-3 py-2 rounded-xl"
                required
                value={kategori}
                onChange={(e) => {
                  setKategori(e.target.value);
                }}
              >
                <option value="">Pilih Kategori</option>
                {category ? (
                  category.map((item) => {
                    return (
                      <option key={item.id} value={item.id}>
                        {item.categoryName}
                      </option>
                    );
                  })
                ) : (
                  <></>
                )}
              </select>
            </FormGroup>
            {/* End Of Form Untuk Kategori */}

            {/* Form Untuk Deskripsi */}
            <FormGroup className="mb-8">
              <Label className="block mb-3">Deskripsi</Label>
              <textarea
                placeholder="Contoh: Jalan Ikan Hiu 33"
                className="w-full border-2 px-3 py-2 outline-none overflow-auto h-24 rounded-xl"
                type="textarea"
                value={deskripsi}
                onChange={(e) => {
                  setDeskripsi(e.target.value);
                }}
                required
              ></textarea>
            </FormGroup>
            {/* End Of Form Untuk Deskripsi */}

            {/* Form Untuk Input Gambar Produk */}
            <FormGroup className="mb-8">
              <Label className="block mb-3">Foto Produk</Label>
              <div className="flex items-center gap-6">
                <label
                  className={`w-24 ${
                    image.length > 3 ? "cursor-deault" : "cursor-pointer"
                  } relative h-24 border-dashed border-2 border-[#d0d0d0] bg-[#fff] rounded-xl flex items-center justify-center`}
                >
                  <input
                    type="file"
                    className="absolute -z-10 w-10 h-10"
                    accept="image/*"
                    onChange={hanldechangeUpload}
                    disabled={image.length > 3 ? true : false}
                  />
                  <FiPlus className="my-auto text-3xl text-[#8a8a8a]" />
                </label>

                <div>
                  <h1 className="text-2xl text-gray-400">{`${image.length}/4 Photos`}</h1>
                  {/* Warning Jumlah Foto */}
                  {image.length > 3 ? (
                    <p className="text-sm text-red-500 font-light">
                      Max Of Photos
                    </p>
                  ) : (
                    <></>
                  )}
                  {/* Warning Jumlah Foto */}

                  {/* Warning Size Foto */}
                  {errSizeImg ? (
                    <p className="text-sm text-red-500 font-light">
                      {errSizeImg}
                    </p>
                  ) : (
                    <></>
                  )}
                  {/* Warning Size Foto */}
                </div>
              </div>

              {image.map((item, index) => {
                return (
                  <div
                    key={index}
                    className="flex justify-between mt-12 items-center border-b pb-2"
                  >
                    <img
                      src={item.url}
                      alt={`gambar ${item.id}`}
                      className="w-20 h-20 rounded-md border object-cover"
                    />
                    <FiX
                      className="text-red-500 text-xl cursor-pointer"
                      onClick={() => removeImage(index)}
                    />
                  </div>
                );
              })}
            </FormGroup>
            {/* End Of Form Untuk Input Gambar Produk */}

            {/* Button Submit and Preview */}
            <div className="grid grid-cols-2 gap-4">
              <button
                className="cursor-pointer flex items-center justify-center
              bg-white border border-btnPrimary text-black rounded-md px-8 py-2"
                onClick={seePreview}
              >
                Preview
              </button>

              <input
                className="cursor-pointer flex items-center justify-center bg-btnPrimary text-white rounded-md px-8 py-2"
                type="submit"
                value="Terbitkan"
              />
            </div>
            {/* Button Submit and Preview */}
            {isempty ? (
              <p className="mt-3 text-red-500 text-sm">
                *Please complete the form
              </p>
            ) : (
              <></>
            )}
          </form>
        </div>
      </div>
    </>
  );
}

export default UpdateProdukPage;

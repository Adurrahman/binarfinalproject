import Toast from "../../Component/Toast/Toast";
import React, { useEffect, useState } from "react";
import Avatar from "../../Component/Avatar/Avatar";
import ContainerCard from "../../Component/ContainerCard/ContainerCard";
import Navbar from "../../Component/Navbar/Navbar";
import SellerNavCategory from "../../Component/SellerNavCategory/SellerNavCategory";
import {
  changeStatus,
  getAllProductsSeller,
  getAllProductsTerjual,
  getAllProductsTertawar,
} from "../../Store/productSlice";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../../Component/Loader/Loader";
import emptyProduct from "../../asset/emptyProduct.png";
import { useNavigate, useParams } from "react-router-dom";
import Cardku from "../../Component/Card/CustomCard";
import { Link } from "react-router-dom";
import { changeStatusUser } from "../../Store/authSlice";

function ProductTerjual() {
  const productsSeller = useSelector((state) => state.product.productsSeller);
  const checkLoading = useSelector((state) => state.product.loading);
  const error = useSelector((state) => state.product.isError);
  const [title, setTitle] = useState("Dashboard Seller | RangKas");

  useEffect(() => {
    document.title = title;
  }, [title]);

  let { category } = useParams();

  if (category === "diminati" || category === "terjual") {
    return (
      <>
        {checkLoading && <Loader />}
        {!checkLoading && productsSeller ? (
          <div className="px-4 bg-white w-full pb-20 grid grid-cols-2 gap-y-5 gap-x-3 lg:grid-cols-3 lg:basis-3/4 mt-10 lg:mt-0">
            {productsSeller ? (
              productsSeller.map((item) => {
                return (
                  <Link
                    key={item.product_id}
                    to={`/edit-product/${item.product_id}`}
                  >
                    <Cardku Product={item} id={item.product_id} />
                  </Link>
                );
              })
            ) : (
              <></>
            )}
          </div>
        ) : (
          <></>
        )}
        {!checkLoading && error ? (
          <div className="w-full relative top-16 h-[20rem] lg:top-32">
            <img src={emptyProduct} className="block mx-auto" />
            <div className="w-80 h-20 mx-auto mt-3">
              <p className="text-center">
                Kategori yang kamu cari belum ada nih
              </p>
            </div>
          </div>
        ) : (
          <></>
        )}
      </>
    );
  } else if (category === undefined) {
    return (
      <>
        {checkLoading && <Loader />}
        {!checkLoading && productsSeller ? (
          <ContainerCard data={productsSeller} />
        ) : (
          <ContainerCard />
        )}
      </>
    );
  }
}

function DaftarJual() {
  const statusUser = useSelector((state) => state.auth.status);
  const statusproduct = useSelector((state) => state.product.status);
  const userLocal = localStorage.getItem("user");
  const userData = JSON.parse(userLocal);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  let { category } = useParams();

  useEffect(() => {
    if (category === "diminati") {
      dispatch(getAllProductsTertawar());
    } else if (category === "terjual") {
      dispatch(getAllProductsTerjual());
    } else if (category === undefined) {
      dispatch(getAllProductsSeller());
    }
  }, [dispatch, category]);

  useEffect(() => {
    if (statusUser === "succeeded") {
      setTimeout(() => {
        dispatch(changeStatusUser());
      }, 2300);
    }
  }, [statusUser]);

  useEffect(() => {
    if (statusproduct === "succeeded") {
      setTimeout(() => {
        dispatch(changeStatus());
      }, 2300);
    }
  }, [statusproduct]);

  useEffect(() => {
    if (userData.is_valid_user == false) {
      navigate("/edit-profile");
    }
  }, [userData]);

  return (
    <>
      <Navbar Text={"Daftar Jual Saya"} />

      {statusUser === "succeeded" ? <Toast>Edit Profile success</Toast> : <></>}
      {statusproduct === "succeeded" ? (
        <Toast>Add Product success</Toast>
      ) : (
        <></>
      )}

      <div className="w-full relative px-4 top-20 lg:px-32 lg:top-28">
        <Avatar editable={true} />
      </div>

      <div className="relative top-28 lg:px-32 lg:flex lg:top-36">
        <SellerNavCategory />
        <ProductTerjual />
      </div>
    </>
  );
}

export default DaftarJual;

import React, { useEffect, useState } from "react";
import ProductCard from "../../Component/Product/ProductCard/ProductCard";
import ProductDescription from "../../Component/Product/Description/Description";
import Button from "../../Component/Button/Button";
import Avatar from "../../Component/Avatar/Avatar";
import Slider from "react-slick";
import { useDispatch, useSelector } from "react-redux";
import { FiArrowLeft } from "react-icons/fi";
import { Link, useNavigate } from "react-router-dom";
import { addNewProduct, changeStatus } from "../../Store/productSlice";
import Loader from "../../Component/Loader/Loader";

const ProductPage = () => {
  const checkLoading = useSelector((state) => state.product.loading);
  const status = useSelector((state) => state.product.status);
  const editableProduct = useSelector((state) => state.product.newProduct);
  const [showableImage, setShowableImage] = useState([]);
  const imgData = editableProduct.getAll("images");
  let navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    setShowableImage(
      imgData.map((element) => {
        return URL.createObjectURL(element);
      })
    );
  }, []);

  useEffect(() => {
    if (status === "succeeded") {
      dispatch(changeStatus());
      navigate("/dashboard-seller", { replace: true });
    }
  }, []);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    appendDots: (dots) => (
      <div
        style={{
          position: "absolute",
          bottom: "50px",
        }}
      >
        <ul style={{ margin: "0px" }}> {dots} </ul>
      </div>
    ),
  };
  return (
    <>
      {checkLoading ? <Loader /> : <></>}

      <div className="flex items-center absolute z-10 top-6 w-full px-4">
        <Link to="/add-new-product">
          <FiArrowLeft className="text-2xl bg-white rounded-full" />
        </Link>
      </div>
      <div>
        <Slider {...settings}>
          {showableImage.map((item, index) => {
            return (
              <div className="h-[400px] w-full" key={index}>
                <img
                  className="w-full h-full object-cover"
                  src={item}
                  alt={`img_procuct ${index}`}
                />
              </div>
            );
          })}
        </Slider>
      </div>
      <div className="w-11/12 relative inset-x-0 mx-auto flex flex-col gap-4 z-10">
        <ProductCard
          dataProduct={editableProduct}
          containerClass={
            "w-full mt-[-2.5rem] mx-auto bg-white rounded-2xl p-4 border"
          }
        />
        <Avatar />
        <ProductDescription
          desc={editableProduct.get("description")}
          containerClass={"p-4 border rounded-2xl mb-24"}
        />
        <Button
          className={"bottom-6 fixed inset-x-28"}
          children={<p>Terbitkan</p>}
          onClick={() => {
            dispatch(addNewProduct(editableProduct));
          }}
        />
      </div>
    </>
  );
};

export default ProductPage;

import React, { useEffect } from "react";
import ProductDescription from "../../Component/Product/Description/Description";
import Button from "../../Component/Button/Button";
import Loader from "../../Component/Loader/Loader";
import Avatar from "../../Component/Avatar/Avatar";
import Tawar from "../../Component/Modal/Tawar";
import Slider from "react-slick";
import { FiArrowLeft } from "react-icons/fi";
import { Link, useParams } from "react-router-dom";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getProductByID } from "../../Store/productSlice";
import { getAllCategory } from "../../Store/categorySlice";
import { getUserById } from "../../Store/authSlice";
import { changeStatusOffer } from "../../Store/offerSlice";
import Toast from "../../Component/Toast/Toast";
import imgError from "../../asset/emptyProduct.png";

const ProductPageBuyer = () => {
  let { idProduct } = useParams();
  const product = useSelector((state) => state.product.product);
  const checkLoadingProduct = useSelector((state) => state.product.loading);
  const checkLoadingCategory = useSelector((state) => state.product.loading);
  const checkLoadingSellerUser = useSelector((state) => state.product.loading);
  const error = useSelector((state) => state.product.isError);
  const checkLoadingOffer = useSelector((state) => state.offer.loading);
  const statusOffer = useSelector((state) => state.offer.status);
  const category = useSelector((state) => state.category.category);
  const sellerUser = useSelector((state) => state.auth.sellerUser);
  const [title, setTitle] = useState("");

  useEffect(() => {
    document.title = product
      ? product.product_name + " | RangKas"
      : "Loading... ";
  }, [title, product]);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllCategory());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getProductByID(idProduct));
  }, [dispatch, idProduct]);

  useEffect(() => {
    {
      product && dispatch(getUserById(product.user_id));
    }
  }, [dispatch, product]);

  const getCategoryName = () => {
    let a = category.find((item) => {
      if (product && product.category_id.toString() === item.id.toString()) {
        return item;
      }
    });
    return a ? a.categoryName : "Other";
  };

  const [isModalOpen, setModal] = useState(false);

  function clicked(data) {
    setModal(data);
  }

  useEffect(() => {
    if (statusOffer === "succeeded") {
      setTimeout(() => {
        dispatch(changeStatusOffer());
      }, 2300);
    }
  }, [statusOffer]);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    appendDots: (dots) => (
      <div
        style={{
          position: "absolute",
          bottom: "50px",
        }}
      >
        <ul style={{ margin: "0px" }}> {dots} </ul>
      </div>
    ),
  };

  if (
    (checkLoadingProduct &&
      checkLoadingCategory &&
      checkLoadingSellerUser &&
      !product &&
      !category &&
      !sellerUser) ||
    checkLoadingOffer
  ) {
    return <Loader />;
  } else if (
    (error && !product) ||
    (error && !category) ||
    (error && !sellerUser)
  ) {
    return (
      <div className="relative top-40 mx-20">
        <Link to="/">
          <FiArrowLeft className="text-2xl text-btnPrimary bg-bgColorSecond rounded-full" />
        </Link>
        <img
          src={imgError}
          alt="error"
          className="w-[200px] block mx-auto mt-10"
        />
        <h1 className="text-center text-md mt-8 text-btnPrimary">
          Maaf produk yang kamu cari tidak ditemukan
        </h1>
      </div>
    );
  } else if (product && category && sellerUser) {
    return (
      <>
        {statusOffer === "succeeded" ? (
          <Toast>Penawaran mu sudah terkirim</Toast>
        ) : (
          <></>
        )}

        <Tawar isModalOpen={isModalOpen} isModalClose={clicked} />
        <div>
          <div className="flex items-center absolute z-10 top-6 w-full px-4">
            <Link to="/">
              <FiArrowLeft className="text-2xl bg-white rounded-full" />
            </Link>
          </div>
          <div>
            <Slider {...settings}>
              {product ? (
                product.images.map((item) => {
                  return (
                    <div className="h-[400px] w-full">
                      <img
                        className="w-full h-full object-cover"
                        src={item.url}
                        alt=""
                      />
                    </div>
                  );
                })
              ) : (
                <></>
              )}
            </Slider>
          </div>
          <div className="w-11/12 relative inset-x-0 mx-auto flex flex-col gap-4 z-10">
            <div className="w-full mt-[-2.5rem] mx-auto bg-white rounded-2xl p-4 border">
              <div className="py-2 flex flex-col gap-2">
                <p className="font-bold">
                  {product ? product.product_name : ""}
                </p>
                <p className="font-light text-gray-400">
                  {category ? getCategoryName() : <></>}
                </p>
              </div>
              <p className="font-medium md:py-4">
                Rp {product ? product.price.toLocaleString() : ""}
              </p>
            </div>
            <Avatar userSeller={sellerUser} />
            <ProductDescription
              desc={product ? product.description : ""}
              containerClass={"p-4 border rounded-2xl mb-24"}
            />
            <Button
              onClick={() => clicked(true)}
              className={"bottom-6 fixed inset-x-[4%]"}
              children={<p>Saya tertarik dan ingin nego</p>}
            />
          </div>
        </div>
      </>
    );
  }
};

export default ProductPageBuyer;

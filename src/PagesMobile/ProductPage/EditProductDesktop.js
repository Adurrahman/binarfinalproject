import React, { useEffect, useRef } from "react";
import Avatar from "../../Component/Avatar/Avatar";
import ProductDescription from "../../Component/Product/Description/Description";
import Button from "../../Component/Button/Button";
import { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteProduct,
  delPreviewEditProduct,
  getProductByID,
  previewEditProduct,
  updateProduct,
} from "../../Store/productSlice";
import { getAllCategory } from "../../Store/categorySlice";
import { getUserById } from "../../Store/authSlice";
import Loader from "../../Component/Loader/Loader";
import Slider from "react-slick";
import { FaChevronCircleRight, FaChevronCircleLeft } from "react-icons/fa";
import { changeStatusOffer } from "../../Store/offerSlice";
import axios from "axios";

const ProductCardButton = () => {
  let { idProduct } = useParams();
  const editableproduct = useSelector((state) => state.product.editProduct);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const terapkan = () => {
    const data = new FormData();
    data.append("product_id", editableproduct.product_id);
    data.append("product_uuid", editableproduct.product_uuid);
    data.append("product_name", editableproduct.product_name);
    data.append("price", editableproduct.price);
    data.append("category_id", editableproduct.category_id);
    data.append("description", editableproduct.description);
    data.append("user_id", editableproduct.user_id);
    editableproduct.newImage.forEach((index) => {
      data.append("images", index.url);
    });
    editableproduct.unchanged_url.forEach((index) => {
      data.append("unchanged_url", index.url);
    });

    dispatch(delPreviewEditProduct());
    dispatch(updateProduct(data));
  };

  const screenWidth = window.innerWidth;
  if (screenWidth < 1024) {
    return <></>;
  } else {
    return (
      <div className="flex flex-col gap-3">
        <Button
          type="primary"
          children={<p>Terapkan Perubahan</p>}
          onClick={terapkan}
        />
        <div className="w-full flex items-center justify-between gap-2">
          <Button
            type="outline"
            children={<p>Edit</p>}
            className="w-full"
            onClick={() => {
              navigate(`/update-product/${idProduct}`);
            }}
          />
          <Button
            className="w-full"
            type="danger"
            children={<p>Delete</p>}
            onClick={() => {
              dispatch(deleteProduct(idProduct));
              navigate("/dashboard-seller");
            }}
          />
        </div>
      </div>
    );
  }
};

const EditProductDesktop = () => {
  let { idProduct } = useParams();
  let navigate = useNavigate();
  const editableproduct = useSelector((state) => state.product.editProduct);
  const status = useSelector((state) => state.product.status);
  const checkLoadingCategory = useSelector((state) => state.product.loading);
  const checkLoadingSellerUser = useSelector((state) => state.product.loading);
  const statusOffer = useSelector((state) => state.offer.status);
  const category = useSelector((state) => state.category.category);
  const sellerUser = useSelector((state) => state.auth.sellerUser);
  const dispatch = useDispatch();
  const [title, setTitle] = useState("");
  const [loadingState, setLoadingState] = useState(false);

  useEffect(() => {
    if (status === "succeeded") {
      navigate("/dashboard-seller", { replace: true });
    }
  }, [status, navigate]);

  useEffect(() => {
    document.title = editableproduct
      ? editableproduct.product_name + " | RangKas"
      : "Loading... ";
  }, [title, editableproduct]);

  useEffect(() => {
    {
      editableproduct && dispatch(getUserById(editableproduct.user_id));
    }
  }, [dispatch, editableproduct]);

  useEffect(() => {
    dispatch(getAllCategory());
  }, [dispatch]);

  useEffect(() => {
    if (!editableproduct) {
      setLoadingState(true);
      axios
        .get(
          `https://secondhand-serenades.herokuapp.com/api/v1/product/show-product-by-id/${idProduct}`
        )
        .then((res) => {
          dispatch(
            previewEditProduct({
              ...res.data.data,
              unchanged_url: res.data.data.images,
              newImage: [],
            })
          );
          setLoadingState(false);
        });
    } else if (editableproduct && editableproduct.product_id != idProduct) {
      setLoadingState(true);
      axios
        .get(
          `https://secondhand-serenades.herokuapp.com/api/v1/product/show-product-by-id/${idProduct}`
        )
        .then((res) => {
          dispatch(
            previewEditProduct({
              ...res.data.data,
              unchanged_url: res.data.data.images,
              newImage: [],
            })
          );
          setLoadingState(false);
        });
    }
  }, [dispatch, idProduct, editableproduct]);

  const getCategoryName = () => {
    let a = category
      ? category.find((item) => {
          if (
            editableproduct &&
            editableproduct.category_id.toString() === item.id.toString()
          ) {
            return item;
          }
        })
      : "";
    return a ? a.categoryName : "Other";
  };

  const slider = useRef(null);

  const settings = {
    infinite: true,
    speed: 200,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  useEffect(() => {
    if (statusOffer === "succeeded") {
      setTimeout(() => {
        dispatch(changeStatusOffer());
      }, 2300);
    }
  }, [statusOffer]);
  return (
    <>
      {(loadingState &&
        checkLoadingCategory &&
        checkLoadingSellerUser &&
        !editableproduct &&
        !category &&
        !sellerUser) ||
      checkLoadingCategory ? (
        <Loader />
      ) : (
        <>
          <div className="flex justify-center gap-8 w-full relative top-28 py-4">
            <div className="flex flex-col gap-4">
              <div className="w-[600px] h-[436px]">
                <div
                  style={{ width: "inherit", height: "inherit" }}
                  className="px-8 absolute justify-between items-center z-10 hidden md:inline-flex"
                >
                  <button onClick={() => slider?.current?.slickPrev()}>
                    <FaChevronCircleLeft size={30} color="white" />
                  </button>
                  <button onClick={() => slider?.current?.slickNext()}>
                    <FaChevronCircleRight size={30} color="white" />
                  </button>
                </div>
                <Slider {...settings} ref={slider}>
                  {editableproduct ? (
                    editableproduct.images.map((item) => {
                      return (
                        <img
                          key={item.id}
                          className="w-[600px] h-[436px] object-cover rounded-2xl"
                          src={item.url}
                        />
                      );
                    })
                  ) : (
                    <></>
                  )}
                </Slider>
              </div>

              <ProductDescription
                desc={editableproduct ? editableproduct.description : ""}
                containerClass={"w-[600px] p-6 rounded-2xl border"}
              />
            </div>
            <div className="w-[336px] flex flex-col gap-6">
              <div className="flex flex-col py-4 justify-center border w-[336px] rounded-2xl p-4">
                <div className="py-2 flex flex-col gap-2">
                  <p className="font-bold">
                    {editableproduct ? editableproduct.product_name : ""}
                  </p>
                  <p className="font-light text-gray-400">
                    {getCategoryName()}
                  </p>
                </div>
                <p className="font-medium md:py-4">
                  Rp{" "}
                  {editableproduct
                    ? editableproduct.price.toLocaleString()
                    : ""}
                </p>
                <ProductCardButton />
              </div>
              <Avatar userSeller={sellerUser} />
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default EditProductDesktop;

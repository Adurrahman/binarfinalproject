import React, { useEffect } from "react";
import ProductDescription from "../../Component/Product/Description/Description";
import Button from "../../Component/Button/Button";
import Loader from "../../Component/Loader/Loader";
import Avatar from "../../Component/Avatar/Avatar";
import Slider from "react-slick";
import { FiArrowLeft, FiEdit, FiTrash2 } from "react-icons/fi";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteProduct,
  delPreviewEditProduct,
  previewEditProduct,
  updateProduct,
} from "../../Store/productSlice";
import { getAllCategory } from "../../Store/categorySlice";
import { getUserById } from "../../Store/authSlice";
import axios from "axios";

const EditProductMobile = () => {
  let { idProduct } = useParams();
  const navigate = useNavigate();

  const editableproduct = useSelector((state) => state.product.editProduct);
  const checkLoadingCategory = useSelector((state) => state.product.loading);
  const checkLoadingSellerUser = useSelector((state) => state.product.loading);
  const error = useSelector((state) => state.product.isError);
  const status = useSelector((state) => state.product.status);
  const category = useSelector((state) => state.category.category);
  const sellerUser = useSelector((state) => state.auth.sellerUser);
  const [title, setTitle] = useState("");
  const [loadingState, setLoadingState] = useState(false);

  useEffect(() => {
    document.title = editableproduct
      ? editableproduct.product_name + " | RangKas"
      : "Loading... ";
  }, [title, editableproduct]);

  const changeTitle = (event) => {
    setTitle(event.target.value);
  };

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllCategory());
  }, [dispatch]);

  useEffect(() => {
    if (!editableproduct) {
      setLoadingState(true);
      axios
        .get(
          `https://secondhand-serenades.herokuapp.com/api/v1/product/show-product-by-id/${idProduct}`
        )
        .then((res) => {
          dispatch(
            previewEditProduct({
              ...res.data.data,
              unchanged_url: res.data.data.images,
              newImage: [],
            })
          );
          setLoadingState(false);
        });
    } else if (editableproduct && editableproduct.product_id != idProduct) {
      setLoadingState(true);
      axios
        .get(
          `https://secondhand-serenades.herokuapp.com/api/v1/product/show-product-by-id/${idProduct}`
        )
        .then((res) => {
          dispatch(
            previewEditProduct({
              ...res.data.data,
              unchanged_url: res.data.data.images,
              newImage: [],
            })
          );
          setLoadingState(false);
        });
    }
  }, [dispatch, idProduct, editableproduct]);

  useEffect(() => {
    {
      editableproduct && dispatch(getUserById(editableproduct.user_id));
    }
  }, [dispatch, editableproduct]);

  const getCategoryName = () => {
    let a = category.find((item) => {
      if (
        editableproduct &&
        editableproduct.category_id.toString() === item.id.toString()
      ) {
        return item;
      }
    });
    return a ? a.categoryName : "Other";
  };

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    appendDots: (dots) => (
      <div
        style={{
          position: "absolute",
          bottom: "50px",
        }}
      >
        <ul style={{ margin: "0px" }}> {dots} </ul>
      </div>
    ),
  };

  useEffect(() => {
    if (status === "succeeded") {
      navigate("/dashboard-seller", { replace: true });
    }
  }, [status, navigate]);

  const terapkan = () => {
    const data = new FormData();
    data.append("product_id", editableproduct.product_id);
    data.append("product_uuid", editableproduct.product_uuid);
    data.append("product_name", editableproduct.product_name);
    data.append("price", editableproduct.price);
    data.append("category_id", editableproduct.category_id);
    data.append("description", editableproduct.description);
    data.append("user_id", editableproduct.user_id);
    editableproduct.newImage.forEach((index) => {
      data.append("images", index.url);
    });
    editableproduct.unchanged_url.forEach((index) => {
      data.append("unchanged_url", index.url);
    });

    dispatch(delPreviewEditProduct());
    dispatch(updateProduct(data));
  };

  return (
    <>
      {(loadingState &&
        checkLoadingCategory &&
        checkLoadingSellerUser &&
        editableproduct &&
        !category &&
        !sellerUser) ||
      checkLoadingCategory ? (
        <Loader fullpage={true} />
      ) : (
        <>
          <div>
            <div className="flex items-center absolute z-10 top-6 w-full px-4">
              <Link to="/dashboard-seller">
                <FiArrowLeft className="text-2xl bg-white rounded-full" />
              </Link>
            </div>
            <div>
              <Slider {...settings}>
                {editableproduct ? (
                  editableproduct.images.map((item) => {
                    return (
                      <div className="h-[400px] w-full" key={item.id}>
                        <img
                          className="w-full h-full object-cover"
                          src={item.url}
                        />
                      </div>
                    );
                  })
                ) : (
                  <></>
                )}
              </Slider>
            </div>
            <div className="w-11/12 relative inset-x-0 mx-auto flex flex-col gap-4 z-10">
              <div className="w-full mt-[-2.5rem] mx-auto bg-white rounded-2xl p-4 border">
                <div className="py-2 flex flex-col gap-2">
                  <p className="font-bold">
                    {editableproduct ? editableproduct.product_name : ""}
                  </p>
                  <p className="font-light text-gray-400">
                    {category ? getCategoryName() : <></>}
                  </p>
                </div>
                <p className="font-medium md:py-4">
                  Rp{" "}
                  {editableproduct
                    ? editableproduct.price.toLocaleString()
                    : ""}
                </p>
              </div>
              <Avatar userSeller={sellerUser} />
              <ProductDescription
                desc={editableproduct ? editableproduct.description : ""}
                containerClass={"p-4 border rounded-2xl mb-40"}
              />

              <div className="w-full fixed bottom-4 left-0 right-0 px-4">
                <div className="w-full flex items-center justify-end gap-2 mb-2">
                  <div
                    className="cursor-pointer bg-white border border-btnPrimary text-black rounded-[100%]"
                    onClick={() => {
                      navigate(`/update-product/${idProduct}`);
                    }}
                  >
                    <FiEdit className="text-2xl m-2 text-btnPrimary" />
                  </div>

                  <div
                    className="cursor-pointer  border-red-500 border bg-red-500 text-white rounded-[100%]"
                    onClick={() => {
                      dispatch(deleteProduct(idProduct));
                      navigate("/dashboard-seller");
                    }}
                  >
                    <FiTrash2 className="text-2xl m-2" />
                  </div>
                </div>

                <Button
                  className="w-full"
                  children={<p>Terapkan Perubahan</p>}
                  onClick={terapkan}
                />
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default EditProductMobile;

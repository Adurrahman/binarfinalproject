import React, { useEffect } from "react";
import { useState } from "react";
import { AiOutlineEye } from "react-icons/ai";
import LoginBg from "../../asset/Rectangle 133.png";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { cleanError, userLogin, userRegister } from "../../Store/authSlice";
import { FiArrowLeft } from "react-icons/fi";
import Loader from "../../Component/Loader/Loader";

const Login = ({ pageStatus }) => {
  const checkLoading = useSelector((state) => state.auth.loading);
  const error = useSelector((state) => state.auth.isError);
  const user = useSelector((state) => state.auth.user);
  const localUser = localStorage.getItem("user");
  const token = localStorage.getItem("token");
  let navigate = useNavigate();
  const dispatch = useDispatch();
  const [title, setTitle] = useState("");

  useEffect(() => {
    document.title = (pageStatus ? "Register" : "Login") + " | RangKas";
  }, [title, pageStatus]);

  const changeTitle = (event) => {
    setTitle(event.target.value);
  };

  useEffect(() => {
    if (!checkLoading && localUser && token) {
      navigate("/");
    }
  }, [checkLoading, localUser, token, navigate]);

  const [inputRegister, setInputRegister] = useState({
    name: "",
    email: "",
    password: "",
  });

  const [inputLogin, setInputLogin] = useState({
    email: "",
    password: "",
  });

  const [passwordShow, setPasswordShow] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(true);

  const handleInputRegister = (e) => {
    setInputRegister({
      ...inputRegister,
      [e.target.id]: e.target.value,
    });
  };

  const handleInputLogin = (e) => {
    setInputLogin({
      ...inputLogin,
      [e.target.id]: e.target.value,
    });
  };

  const PasswordValidation = () => {
    const regEx2 = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/;
    if (!regEx2.test(inputRegister.password)) {
      setIsPasswordValid(false);
    } else {
      setIsPasswordValid(true);
      dispatch(userRegister(inputRegister));
    }
  };

  const handleSubmitRegister = (e) => {
    e.preventDefault();
    PasswordValidation();
  };

  const handleSubmitLogin = (e) => {
    e.preventDefault();
    dispatch(userLogin(inputLogin));
  };

  const tooglePasswordHandle = () => {
    setPasswordShow(!passwordShow);
  };

  return (
    <>
      {checkLoading ? <Loader /> : <></>}
      <div className=" grid grid-cols-1 h-screen md:grid-cols-2">
        <div className="flex items-center fixed z-10 top-6 w-full px-4 lg:hidden">
          <Link to="/">
            <FiArrowLeft className="text-2xl" />
          </Link>
        </div>
        <img className="w-11/12 h-screen object-cover hidden md:block" src={LoginBg} alt="" />
        <div className="pt-20 flex flex-col gap-2 h-screen md:justify-center md:pt-0">
          {pageStatus ? (
            /// Start of Register Form ///
            <>
              <p className="font-bold text-2xl pl-4 pb-4 md:pl-32">Daftar</p>

              <form onSubmit={handleSubmitRegister} className="relative">
                <div className="px-4 md:px-32">
                  {user ? <h1 className=" text-green-500 pr-20 mb-6">Registrasi Berhasil, Silahkan Login Untuk Mulai Bertransaksi</h1> : <></>}

                  {/* Handle Error Email Sama */}
                  {error ? <p className="mb-3 text-red-400 text-sm font-light">email telah di gunakan.</p> : <></>}

                  <label htmlFor="name" className="text-sm font-semibold">
                    Nama
                  </label>
                  <input
                    required
                    type="text"
                    className=" rounded-xl
                  w-full px-3 py-2 border border-gray-300
                  placeholder-gray-300 text-gray-900 mb-4
                  focus:outline-none focus:ring-btnPrimary
                  focus:border-btnPrimary focus:z-10 sm:text-sm"
                    placeholder="Nama Lengkap"
                    id="name"
                    value={inputRegister.name}
                    onChange={handleInputRegister}
                  />
                </div>
                <div className="px-4 md:px-32">
                  <label htmlFor="email" className="text-sm font-semibold">
                    Email
                  </label>
                  <input
                    required
                    type="email"
                    className=" rounded-xl
                  w-full px-3 py-2 border border-gray-300
                  placeholder-gray-300 text-gray-900 mb-4
                  focus:outline-none focus:ring-btnPrimary
                  focus:border-btnPrimary focus:z-10 sm:text-sm"
                    placeholder="Contoh: johndee@gmail.com"
                    id="email"
                    value={inputRegister.email}
                    onChange={handleInputRegister}
                  />
                </div>
                <div className="px-4 md:px-32">
                  <label htmlFor="password" className="text-sm font-semibold">
                    Buat Password
                  </label>
                  <div className="relative">
                    <input
                      required
                      type={passwordShow ? "text" : "password"}
                      className=" rounded-xl
            w-full px-3 py-2 border border-gray-300
            placeholder-gray-300 text-gray-900 mb-4
            focus:outline-none focus:ring-btnPrimary
            focus:border-btnPrimary focus:z-10 sm:text-sm"
                      placeholder="Masukkan Password"
                      id="password"
                      value={inputRegister.password}
                      onChange={handleInputRegister}
                    />
                    <AiOutlineEye className="absolute inset-y-0 top-[0.3rem] right-4 text-3xl text-gray-400" onClick={tooglePasswordHandle} />

                    {isPasswordValid ? <></> : <span className="text-sm text-[#ff0000] relative -top-2">"Password must be 8 character with some capital alphabet and number"</span>}
                  </div>
                  <input
                    disabled={checkLoading}
                    value="Register"
                    type="submit"
                    className={`${checkLoading ? "cursor-default bg-bgColorSecond" : "cursor-pointer bg-btnPrimary"} flex items-center justify-center text-white rounded-xl px-8 py-3 mt-10 w-full`}
                  />
                </div>
              </form>

              <div className="w-full absolute bottom-0 pb-6 md:static md:pt-8 md:pb-0">
                <p className="text-sm font-semibold text-center">
                  Sudah punya akun?
                  {checkLoading ? (
                    <span className="text-[#7126b5]"> Masuk di sini</span>
                  ) : (
                    <Link
                      to="/login"
                      onClick={() => {
                        dispatch(cleanError());
                      }}
                    >
                      <span className="text-[#7126b5]"> Masuk di sini</span>
                    </Link>
                  )}
                </p>
              </div>
            </>
          ) : (
            <>
              {/* FORM UNTUK LOGIN */}
              <p className="font-bold text-2xl pl-4 pb-4 md:pl-32">Masuk</p>
              <form onSubmit={handleSubmitLogin} className="relative">
                <div className="pt-2 px-4 md:px-32">
                  {error ? <p className="mb-3 text-red-400 text-sm font-light">The username or password were incorrect.</p> : <></>}
                  <label htmlFor="email" className="text-sm font-semibold">
                    Email
                  </label>
                  <input
                    required
                    data-testid="emailLogin"
                    type="email"
                    className=" rounded-xl
                  w-full px-3 py-2 border border-gray-300
                  placeholder-gray-300 text-gray-900 mb-4
                  focus:outline-none focus:ring-btnPrimary
                  focus:border-btnPrimary focus:z-10 sm:text-sm"
                    placeholder="Contoh: johndee@gmail.com"
                    id="email"
                    value={inputLogin.email}
                    onChange={handleInputLogin}
                  />
                </div>
                <div className="px-4 md:px-32">
                  <label htmlFor="password" className="text-sm font-semibold">
                    Password
                  </label>
                  <div className="relative">
                    <input
                      data-testid="passLogin"
                      required
                      type={passwordShow ? "text" : "password"}
                      className=" rounded-xl
            w-full px-3 py-2 border border-gray-300
            placeholder-gray-300 text-gray-900 mb-4
            focus:outline-none focus:ring-btnPrimary
            focus:border-btnPrimary focus:z-10 sm:text-sm"
                      placeholder="Masukkan Password"
                      id="password"
                      value={inputLogin.password}
                      onChange={handleInputLogin}
                    />
                    <AiOutlineEye className="absolute inset-y-0 top-[0.3rem] right-4 text-3xl text-gray-400" onClick={tooglePasswordHandle} />
                  </div>
                  <input
                    disabled={checkLoading}
                    value="Login"
                    type="submit"
                    data-testid="submitLogin"
                    className={`${checkLoading ? "cursor-default bg-bgColorSecond" : "cursor-pointer bg-btnPrimary"} flex items-center justify-center text-white rounded-xl px-8 py-3 mt-10 w-full`}
                  />
                </div>
              </form>

              <div className="w-full absolute bottom-0 pb-6 md:static md:pt-8 md:pb-0">
                <p className="text-sm font-semibold text-center">
                  Belum punya akun?
                  {checkLoading ? (
                    <span className="text-[#7126b5]"> Daftar Disini</span>
                  ) : (
                    <Link
                      to="/register"
                      onClick={() => {
                        dispatch(cleanError());
                      }}
                    >
                      <span className="text-[#7126b5]"> Daftar Disini</span>
                    </Link>
                  )}
                </p>
              </div>
              {/* ENF OF FORM LOGIN */}
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default Login;

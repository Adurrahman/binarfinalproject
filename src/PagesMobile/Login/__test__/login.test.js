import React from "react";
import { renderWithProviders } from "../../../test-utils";
import { fireEvent } from "@testing-library/react";
import Login from "../Login";
import { act } from "react-dom/test-utils";
import { BrowserRouter as Router } from "react-router-dom";

it("valid email when login", () => {
  const { getByTestId, container } = renderWithProviders(
    <Router>
      <Login pageStatus={false} />
    </Router>
  );

  act(() => {
    fireEvent.change(getByTestId("emailLogin"), {
      target: { value: "abdurrizqoarra@gmail.com" },
    });
    fireEvent.change(getByTestId("passLogin"), {
      target: { value: "Riko1234" },
    });
  });

  act(() => {
    fireEvent.click(getByTestId("submitLogin"));
  });
});

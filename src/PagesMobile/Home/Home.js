import React, { useEffect, useState } from "react";
import Category from "../../Component/Category/Category";
import Header from "../../Component/Header/Header";
import { useRef } from "react";
import { useDraggable } from "react-use-draggable-scroll";
import Button from "../../Component/Button/Button";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import { FiPlus, FiSearch } from "react-icons/fi";
import ContainerCard from "../../Component/ContainerCard/ContainerCard";
import { useDispatch, useSelector } from "react-redux";
import Navbar from "../../Component/Navbar/Navbar";
import { getAllCategory } from "../../Store/categorySlice";
import { getAllProductsPage } from "../../Store/productSlice";
import { getProductByCategory } from "../../Store/productSlice";
import Loader from "../../Component/Loader/Loader";
import {
  BsFillArrowLeftCircleFill,
  BsFillArrowRightCircleFill,
} from "react-icons/bs";

function Home() {
  const navigate = useNavigate();
  const checkLoading = useSelector((state) => state.category.loading);
  const category = useSelector((state) => state.category.category);
  const dispatch = useDispatch();
  const [pageNumber, setPageNumber] = useState(1);
  const products = useSelector((state) => state.product.products);
  const checkLoading2 = useSelector((state) => state.product.loading);

  const [title, setTitle] = useState("Home | Second Hand");

  useEffect(() => {
    document.title = title;
  }, [title]);

  const changeTitle = (event) => {
    setTitle(event.target.value);
  };

  let { idCategory } = useParams();

  useEffect(() => {
    dispatch(getAllCategory());
  }, [dispatch]);

  const ref = useRef();
  const { events } = useDraggable(ref);

  useEffect(() => {
    if (idCategory) {
      dispatch(getProductByCategory(idCategory));
    } else {
      dispatch(getAllProductsPage(pageNumber));
    }
  }, [dispatch, idCategory, pageNumber]);

  const handleNext = () => {
    setPageNumber(pageNumber + 1);
  };
  const handlePrev = () => {
    if (pageNumber > 1) {
      setPageNumber(pageNumber - 1);
    }
  };

  return (
    <>
      <Navbar />
      <div className="fixed bottom-6 z-40 flex justify-center gap-3 w-full items-center">
        {!products ? (
          <div className="w-[30px] h-[30px]"></div>
        ) : products.hasPrevious ? (
          <button
            onClick={handlePrev}
            className="bg-white rounded-full h-[30px] w-[30px]"
          >
            <BsFillArrowLeftCircleFill size={30} className="text-btnPrimary" />
          </button>
        ) : (
          <div className="w-[30px] h-[30px]"></div>
        )}

        <Button
          className="w-32 shadow-lg shadow-[#9049d1]"
          children={
            <>
              <FiPlus className="mr-2 text-[#fff]" />
              <p>Jual</p>
            </>
          }
          onClick={() => {
            navigate("/dashboard-seller");
          }}
        ></Button>

        {!products ? (
          <div className="w-[30px] h-[30px]"></div>
        ) : products.hasNext ? (
          <button
            onClick={handleNext}
            className="bg-white rounded-full h-[30px] w-[30px]"
          >
            <BsFillArrowRightCircleFill size={30} className="text-btnPrimary" />
          </button>
        ) : (
          <div className="w-[30px] h-[30px]"></div>
        )}
      </div>
      <Header />
      <div className="absolute top-80 px-4 w-full">
        <h1 className="text-lg font-medium mb-5">Telusuri Kategori</h1>
        <div
          className="w-full flex gap-4 overflow-scroll scrollbar-hide mb-10"
          {...events}
          ref={ref}
        >
          <div className="whitespace-nowrap">
            <NavLink
              to={`/`}
              style={({ isActive }) => {
                return {
                  color: isActive ? "white" : "black",
                  backgroundColor: isActive ? "#7126B5" : "#E2D4F0",
                  borderRadius: "12px",
                  display: "inline-flex",
                  alignItems: "center",
                  textAlign: "center",
                  cursor: "pointer",
                };
              }}
            >
              <div className="py-3 px-4">
                <div className="flex gap-2">
                  <FiSearch className="mr-2 text-2xl lg:text-gray-400" />
                  <p>Semua</p>
                </div>
              </div>
            </NavLink>
          </div>
          {category ? (
            category.map((item) => {
              return (
                <Category
                  key={item.id}
                  linkId={item.id}
                  Text={item.categoryName}
                  isSeller={false}
                />
              );
            })
          ) : (
            <></>
          )}
        </div>
        {(checkLoading2 && checkLoading) || checkLoading2 ? (
          <Loader fullpage={false} />
        ) : (
          <></>
        )}
      </div>
      <ContainerCard isSeller={false} data={products && products.data} />
    </>
  );
}

export default Home;

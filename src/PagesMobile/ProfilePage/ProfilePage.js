import React, { useEffect, useRef, useState } from "react";
import { FormGroup, Label, Input } from "reactstrap";
import { FiCamera, FiArrowLeft } from "react-icons/fi";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { userEdit } from "../../Store/authSlice";
import Loader from "../../Component/Loader/Loader";

function ProfilePage() {
  const checkLoading = useSelector((state) => state.auth.loading);
  const status = useSelector((state) => state.auth.status);

  const formResult = useRef(null);
  const navigate = useNavigate();
  const localUser = localStorage.getItem("user");
  const userObj = JSON.parse(localUser);
  const dispatch = useDispatch();

  const [avatarImage, setAvatarImage] = useState(
    userObj ? userObj.image : null
  );
  const [image, setImage] = useState();

  const [nama, setNama] = useState(userObj ? userObj.name : "");
  const [kota, setKota] = useState(userObj ? userObj.city : "");
  const [alamat, setAlamat] = useState(userObj ? userObj.address : "");
  const [noHP, setNoHP] = useState(userObj ? userObj.phoneNumber : "");

  const [cekNoHP, setCekNoHP] = useState(true);

  const dataUser = localStorage.getItem("user");
  const dataUserObj = JSON.parse(dataUser);
  const [title, setTitle] = useState(
    `Change Profile | ${dataUserObj.name} | RangKas`
  );

  useEffect(() => {
    document.title = title;
  }, [title]);

  const changePhone = (e) => [setNoHP(e.target.value.replace(/\D/g, ""))];

  function handlechangeUpload(e) {
    let uploaded = e.target.files[0];
    setImage(uploaded);
    setAvatarImage(URL.createObjectURL(uploaded));
  }

  function handleSubmit(e) {
    e.preventDefault();
    const data = new FormData(formResult.current);
    data.append("id", userObj.id);
    if (noHP.length < 10) {
      setCekNoHP(false);
    } else {
      setCekNoHP(true);
      dispatch(userEdit(data));
    }
  }

  useEffect(() => {
    if (status === "succeeded") {
      navigate("/dashboard-seller", { replace: true });
    }
  }, [status, navigate]);

  return (
    <>
      {checkLoading ? <Loader /> : <></>}
      <div className="w-full overflow-y-hidden p-8 lg:relative lg:top-20 2xl:px-[28rem] lg:px-60">
        {window.innerWidth < 1024 ? (
          <div className="flex items-center w-full lg:hidden justify-between mb-5">
            <FiArrowLeft
              className="text-2xl cursor-pointer"
              onClick={() => {
                dataUserObj.is_valid_user ? navigate(-1) : navigate(-3);
              }}
            />
            <h1 className="font-medium">Lengkapi Info Akun</h1>
            <div className="w-5"></div>
          </div>
        ) : (
          <FiArrowLeft
            className="text-2xl cursor-pointer"
            onClick={() => {
              dataUserObj.is_valid_user ? navigate(-1) : navigate(-3);
            }}
          />
        )}

        <form ref={formResult} className="mb-20" onSubmit={handleSubmit}>
          <label className="w-24 mb-5 mx-auto cursor-pointer relative h-24 bg-bgColorSecond rounded-xl flex items-center justify-center">
            <input
              type="file"
              name="image"
              className="absolute -z-10 w-10 h-10"
              accept="image/*"
              onChange={handlechangeUpload}
              required
            />
            {avatarImage ? (
              <img
                src={avatarImage}
                alt="avatar"
                className="w-full h-full object-cover rounded-xl"
              />
            ) : (
              <FiCamera className="my-auto text-3xl text-btnPrimary" />
            )}
          </label>

          <FormGroup className="mb-8">
            <Label className="block mb-3">
              Nama<span className="text-red-500">*</span>
            </Label>
            <Input
              className="outline-none border-2 px-3 py-2 rounded-xl w-full"
              placeholder="Nama"
              type="text"
              value={nama}
              name="name"
              onChange={(e) => {
                setNama(e.target.value);
              }}
              required
            />
          </FormGroup>

          <FormGroup className="mb-8">
            <Label className="block mb-3">
              Kota<span className="text-red-500">*</span>
            </Label>
            <Input
              className="w-full outline-none border-2 px-3 py-2 rounded-xl"
              type="select"
              value={kota}
              name="city"
              onChange={(e) => {
                setKota(e.target.value);
              }}
              required
            >
              <option value="">Select One</option>
              <option value="Jakarta">Jakarta</option>
              <option value="Malang">Malang</option>
              <option value="Bandung">Bandung</option>
              <option value="Bogor">Bogor</option>
              <option value="Yogyakarta">Yogyakarta</option>
              <option value="bojowongso">Bojowongso</option>
            </Input>
          </FormGroup>

          <FormGroup className="mb-8">
            <Label className="block mb-3">
              Alamat<span className="text-red-500">*</span>
            </Label>
            <Input
              placeholder="Contoh: Jalan Ikan Hiu 33"
              className="w-full border-2 px-3 py-2 outline-none overflow-auto h-24 rounded-xl"
              type="textarea"
              name="address"
              value={alamat}
              onChange={(e) => {
                setAlamat(e.target.value);
              }}
              required
            />
          </FormGroup>

          <FormGroup className="mb-8">
            <Label className="block mb-3">
              No Handphone<span className="text-red-500">*</span>
            </Label>
            <Input
              className="outline-none border-2 px-3 py-2 rounded-xl w-full"
              placeholder="contoh: +628123456789"
              type="number"
              name="phone_number"
              value={noHP}
              onChange={changePhone}
              required
            />
            {cekNoHP ? (
              <></>
            ) : (
              <p className="text-red-500 text-sm mt-2 ml-3">
                Nomer HP Tidak Valid
              </p>
            )}
          </FormGroup>

          <input
            className="cursor-pointer flex items-center justify-center bg-btnPrimary text-white rounded-xl px-8 py-3 mt-10 w-full"
            type="submit"
            value="Simpan"
          />
        </form>
      </div>
    </>
  );
}

export default ProfilePage;

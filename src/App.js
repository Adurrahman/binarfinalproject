import "./App.css";
import Login from "./PagesMobile/Login/Login";
import ProfilePage from "./PagesMobile/ProfilePage/ProfilePage";
import LayoutDesktop from "./Component/Layout/LayoutDesktop";
import Home from "./PagesMobile/Home/Home";
import HomeDesktop from "./PagesDesktop/Home/HomeDesktop";
import InfoProdukPage from "./PagesMobile/InfoProdukPage/InfoProdukPage";
import DaftarJual from "./PagesMobile/DaftarJual/DaftarJual";
import InfoPenawar from "./Component/InfoPenawar/InfoPenawar";
import { Route, Routes } from "react-router-dom";
import ContainerCard from "./Component/ContainerCard/ContainerCard";
import AccountPage from "./PagesMobile/AccountPage/AccountPage";
import ProductPage from "./PagesMobile/ProductPage/ProductPage";
import ProductPageBuyer from "./PagesMobile/ProductPage/ProductPageBuyer";
import ProductPageDesktop from "./PagesDesktop/ProductPageDesktop/ProductPageDesktop";
import ProductPageBuyerDesktop from "./PagesDesktop/ProductPageDesktop/ProductPageBuyerDesktop";
import Notification from "./Component/Notification/Notification";
import Protected from "./Protected";
import EditProductDesktop from "./PagesMobile/ProductPage/EditProductDesktop";
import EditProductMobile from "./PagesMobile/ProductPage/EditProductMobile";
import UpdateProdukPage from "./PagesMobile/InfoProdukPage/UpdateProdukPage";
import ChangePasswordDesktop from "./PagesDesktop/ChangePassword/ChangePasswordDesktop";

function App() {
  const screenWidth = window.innerWidth;
  if (screenWidth < 1024) {
    return (
      <Routes>
        {/* Component Tanpat Layout Navbar */}
        <Route path="/login" element={<Login pageStatus={false} />} />
        <Route path="/register" element={<Login pageStatus={true} />} />
        <Route
          path="/edit-profile"
          element={
            <Protected>
              <ProfilePage />
            </Protected>
          }
        />
        <Route
          path="/add-new-product"
          element={
            <Protected>
              <InfoProdukPage />
            </Protected>
          }
        />
        <Route
          path="/update-product/:idProduct"
          element={
            <Protected>
              <UpdateProdukPage />
            </Protected>
          }
        />
        <Route
          path="/profile"
          element={
            <Protected>
              <AccountPage />
            </Protected>
          }
        />
        <Route
          path="/preview-new-product"
          element={
            <Protected>
              <ProductPage />
            </Protected>
          }
        />
        <Route
          path="/detail-product/:idProduct"
          element={
            <Protected>
              <ProductPageBuyer />
            </Protected>
          }
        />
        <Route
          path="/edit-product/:idProduct"
          element={
            <Protected>
              <EditProductMobile />
            </Protected>
          }
        />
        <Route
          path="/notification"
          element={
            <Protected>
              <Notification />
            </Protected>
          }
        />
        <Route
          path="/info-penawar/:detailNotif"
          element={
            <Protected>
              <InfoPenawar />
            </Protected>
          }
        />

        <Route
          path="/dashboard-seller"
          element={
            <Protected>
              <DaftarJual />
            </Protected>
          }
        >
          <Route
            path="/dashboard-seller/:category"
            element={
              <Protected>
                <DaftarJual />
              </Protected>
            }
          />
        </Route>
        <Route
          path="/change-password"
          element={
            <Protected>
              <ChangePasswordDesktop />
            </Protected>
          }
        />

        <Route path="/" element={<Home />}>
          <Route path="/category/:idCategory" element={<ContainerCard />} />
        </Route>
      </Routes>
    );
  } else {
    return (
      <Routes>
        {/* Component Tanpat Layout Navbar */}
        <Route path="/login" element={<Login pageStatus={false} />} />
        <Route path="/register" element={<Login pageStatus={true} />} />
        {/* Component Tanpat Layout Navbar */}

        {/* Routing Dengan Layout Navbar */}
        <Route path="/" element={<LayoutDesktop />}>
          <Route path="/" element={<HomeDesktop />}>
            <Route path="/category/:idCategory" element={<ContainerCard />} />
          </Route>

          <Route
            path="/dashboard-seller"
            element={
              <Protected>
                <DaftarJual />
              </Protected>
            }
          >
            <Route
              path="/dashboard-seller/:category"
              element={
                <Protected>
                  <DaftarJual />
                </Protected>
              }
            />
          </Route>

          <Route
            path="/preview-new-product"
            element={
              <Protected>
                <ProductPageDesktop />
              </Protected>
            }
          />
          <Route path="/" element={<HomeDesktop />} />
          <Route
            path="/profile"
            element={
              <Protected>
                <AccountPage />
              </Protected>
            }
          />
          <Route
            path="/edit-profile"
            element={
              <Protected>
                <ProfilePage />
              </Protected>
            }
          />
          <Route
            path="/add-new-product"
            element={
              <Protected>
                <InfoProdukPage />
              </Protected>
            }
          />

          <Route
            path="/info-penawar/:detailNotif"
            element={
              <Protected>
                <InfoPenawar />
              </Protected>
            }
          />
          <Route
            path="/detail-product/:idProduct"
            element={
              <Protected>
                <ProductPageBuyerDesktop />
              </Protected>
            }
          />

          <Route
            path="/edit-product/:idProduct"
            element={
              <Protected>
                <EditProductDesktop />
              </Protected>
            }
          />

          <Route
            path="/update-product/:idProduct"
            element={
              <Protected>
                <UpdateProdukPage />
              </Protected>
            }
          />
          <Route
            path="/change-password"
            element={
              <Protected>
                <ChangePasswordDesktop />
              </Protected>
            }
          />
        </Route>

        {/* Routing Dengan Layout Navbar */}
      </Routes>
    );
  }
}

export default App;

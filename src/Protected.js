import React from "react";
import { Navigate } from "react-router-dom";

function Protected({ children }) {
  const dataToken = localStorage.getItem("token");
  const token = JSON.parse(dataToken);

  if (!token) {
    return <Navigate to="/login" />;
  }
  return children;
}

export default Protected;

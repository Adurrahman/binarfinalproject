import React, { useEffect, useState } from "react";
import HeaderDesktop from "../../Component/Header/HeaderDesktop";
import Category from "../../Component/Category/Category";
import { useRef } from "react";
import { useDraggable } from "react-use-draggable-scroll";
import Button from "../../Component/Button/Button";
import { FiPlus, FiSearch } from "react-icons/fi";
import { Navigate, NavLink, useNavigate } from "react-router-dom";
import { getAllCategory } from "../../Store/categorySlice";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import ContainerCard from "../../Component/ContainerCard/ContainerCard";
import { getAllProductsPage } from "../../Store/productSlice";
import { getProductByCategory } from "../../Store/productSlice";
import Loader from "../../Component/Loader/Loader";
import emptyProduct from "../../asset/emptyProduct.png";
import {
  BsFillArrowLeftCircleFill,
  BsFillArrowRightCircleFill,
} from "react-icons/bs";

function Home() {
  let category = useSelector((state) => state.category.category);
  let checkLoading = useSelector((state) => state.category.loading);
  let products = useSelector((state) => state.product.products);
  let checkLoading2 = useSelector((state) => state.product.loading);
  let error2 = useSelector((state) => state.product.isError);
  const token = localStorage.getItem("token");
  const [pageNumber, setPageNumber] = useState(1);
  const [title, setTitle] = useState("Home | RangKas");
  const navigate = useNavigate();
  useEffect(() => {
    document.title = title;
  }, [title]);

  const changeTitle = (event) => {
    setTitle(event.target.value);
  };

  const dispatch = useDispatch();
  let { idCategory } = useParams();

  useEffect(() => {
    dispatch(getAllCategory());
  }, [dispatch]);

  useEffect(() => {
    if (idCategory) {
      dispatch(getProductByCategory(idCategory));
    } else {
      dispatch(getAllProductsPage(pageNumber));
    }
  }, [dispatch, idCategory, pageNumber]);

  const handleNext = () => {
    setPageNumber(pageNumber + 1);
  };
  const handlePrev = () => {
    if (pageNumber > 1) {
      setPageNumber(pageNumber - 1);
    }
  };

  const ref = useRef();
  const { events } = useDraggable(ref);
  return (
    <>
      <div className="fixed bottom-6 z-40 flex justify-center gap-8 w-full">
        {!products ? (
          <div className="w-[40px] h-[40px]"></div>
        ) : products.hasPrevious ? (
          <button onClick={handlePrev} className="bg-white rounded-full">
            <BsFillArrowLeftCircleFill size={40} className="text-btnPrimary" />
          </button>
        ) : (
          <div className="w-[40px] h-[40px]"></div>
        )}

        <Button
          className="w-32 shadow-lg shadow-[#9049d1]"
          children={
            <>
              <FiPlus className="mr-2 text-[#fff]" />
              <p>Jual</p>
            </>
          }
          onClick={() => {
            navigate(token ? "/dashboard-seller" : "/login");
          }}
        ></Button>

        {!products ? (
          <div className="w-[40px] h-[40px]"></div>
        ) : products.hasNext ? (
          <button onClick={handleNext} className="bg-white rounded-full">
            <BsFillArrowRightCircleFill size={40} className="text-btnPrimary" />
          </button>
        ) : (
          <div className="w-[40px] h-[40px]"></div>
        )}
      </div>

      <HeaderDesktop />
      <div className="relative top-36 px-32 w-full">
        <h1 className="text-lg font-bold mb-5">Telusuri Kategori</h1>
        <div
          className="w-full flex gap-4 overflow-x-scroll scrollbar-hide "
          {...events}
          ref={ref}
        >
          <div className="whitespace-nowrap">
            <NavLink
              to={`/`}
              style={({ isActive }) => {
                return {
                  color: isActive ? "white" : "black",
                  backgroundColor: isActive ? "#7126B5" : "#E2D4F0",
                  borderRadius: "12px",
                  display: "inline-flex",
                  alignItems: "center",
                  textAlign: "center",
                  cursor: "pointer",
                };
              }}
            >
              <div className="py-3 px-4">
                <div className="flex gap-2">
                  <FiSearch className="mr-2 text-2xl lg:text-gray-400" />
                  <p>Semua</p>
                </div>
              </div>
            </NavLink>
          </div>

          {category ? (
            category.map((item) => {
              return (
                <Category
                  key={item.id}
                  linkId={item.id}
                  Text={item.categoryName}
                  isSeller={false}
                />
              );
            })
          ) : (
            <></>
          )}
        </div>
        {(checkLoading2 && checkLoading) || checkLoading2 ? (
          <Loader fullpage={false} />
        ) : (
          <></>
        )}
      </div>

      <ContainerCard isSeller={false} data={products && products.data} />
    </>
  );
}

export default Home;

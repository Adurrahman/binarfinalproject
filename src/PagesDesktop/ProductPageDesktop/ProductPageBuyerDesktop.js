import React, { useEffect, useRef } from "react";
import Avatar from "../../Component/Avatar/Avatar";
import ProductDescription from "../../Component/Product/Description/Description";
import Button from "../../Component/Button/Button";
import Tawar from "../../Component/Modal/Tawar";
import { useState } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getProductByID } from "../../Store/productSlice";
import { getAllCategory } from "../../Store/categorySlice";
import { getUserById } from "../../Store/authSlice";
import Loader from "../../Component/Loader/Loader";
import Slider from "react-slick";
import { FaChevronCircleRight, FaChevronCircleLeft } from "react-icons/fa";
import Toast from "../../Component/Toast/Toast";
import { changeStatusOffer } from "../../Store/offerSlice";
import imgError from "../../asset/emptyProduct.png";

const ProductCardButton = ({ clicked }) => {
  const screenWidth = window.innerWidth;
  if (screenWidth < 1024) {
    return <></>;
  } else {
    return (
      <div className="flex flex-col gap-3">
        <Button onClick={() => clicked(true)} type="primary" children={<p>Saya tertarik dan ingin nego</p>} />
      </div>
    );
  }
};

const ProductPageBuyerDesktop = () => {
  let { idProduct } = useParams();
  const product = useSelector((state) => state.product.product);
  const checkLoadingProduct = useSelector((state) => state.product.loading);
  const checkLoadingCategory = useSelector((state) => state.product.loading);
  const checkLoadingSellerUser = useSelector((state) => state.product.loading);
  const checkLoadingOffer = useSelector((state) => state.offer.loading);
  const statusOffer = useSelector((state) => state.offer.status);
  const error = useSelector((state) => state.product.isError);
  const category = useSelector((state) => state.category.category);
  const sellerUser = useSelector((state) => state.auth.sellerUser);
  const dispatch = useDispatch();
  const [title, setTitle] = useState("");

  useEffect(() => {
    document.title = product ? product.product_name + " | RangKas" : "Loading... ";
  }, [title, product]);

  const changeTitle = (event) => {
    setTitle(event.target.value);
  };

  useEffect(() => {
    {
      product && dispatch(getUserById(product.user_id));
    }
  }, [dispatch, product]);

  useEffect(() => {
    dispatch(getAllCategory());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getProductByID(idProduct));
  }, [dispatch, idProduct]);

  const getCategoryName = () => {
    let a = category
      ? category.find((item) => {
          if (product && product.category_id.toString() === item.id.toString()) {
            return item;
          }
        })
      : "";
    return a ? a.categoryName : "Other";
  };

  const [isModalOpen, setModal] = useState(false);
  function clicked(data) {
    setModal(data);
  }

  const slider = useRef(null);

  const settings = {
    infinite: true,
    speed: 200,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  useEffect(() => {
    if (statusOffer === "succeeded") {
      setTimeout(() => {
        dispatch(changeStatusOffer());
      }, 2300);
    }
  }, [statusOffer]);

  if ((checkLoadingProduct && checkLoadingCategory && checkLoadingSellerUser && !product && !category && !sellerUser) || checkLoadingOffer) {
    return <Loader />;
  } else if ((error && !product) || (error && !category) || (error && !sellerUser)) {
    return (
      <div className="relative top-40">
        <img src={imgError} alt="error" className="w-[300px] block mx-auto" />
        <h1 className="text-center text-xl mt-8 text-btnPrimary">Maaf produk yang kamu cari tidak ditemukan</h1>
      </div>
    );
  } else if (product && category && sellerUser) {
    return (
      <>
        {statusOffer === "succeeded" ? <Toast>Penawaran mu sudah terkirim</Toast> : <></>}
        <Tawar isModalOpen={isModalOpen} isModalClose={clicked} sellerUser={sellerUser} />
        <div className="flex justify-center gap-8 w-full relative top-28 py-4">
          <div className="flex flex-col gap-4">
            <div className="w-[600px] h-[436px]">
              <div style={{ width: "inherit", height: "inherit" }} className="px-8 absolute justify-between items-center z-10 hidden md:inline-flex">
                <button onClick={() => slider?.current?.slickPrev()}>
                  <FaChevronCircleLeft size={30} color="white" />
                </button>
                <button onClick={() => slider?.current?.slickNext()}>
                  <FaChevronCircleRight size={30} color="white" />
                </button>
              </div>
              <Slider {...settings} ref={slider}>
                {product ? (
                  product.images.map((item) => {
                    return <img className="w-[600px] h-[436px] object-cover rounded-2xl" src={item.url} />;
                  })
                ) : (
                  <></>
                )}
              </Slider>
            </div>

            <ProductDescription desc={product ? product.description : ""} containerClass={"w-[600px] p-6 rounded-2xl border"} />
          </div>
          <div className="w-[336px] flex flex-col gap-6">
            <div className="flex flex-col py-4 justify-center border w-[336px] rounded-2xl p-4">
              <div className="py-2 flex flex-col gap-2">
                <p className="font-bold">{product ? product.product_name : ""}</p>
                <p className="font-light text-gray-400">{getCategoryName()}</p>
              </div>
              <p className="font-medium md:py-4">Rp {product ? product.price.toLocaleString() : ""}</p>
              <ProductCardButton clicked={clicked} />
            </div>
            <Avatar userSeller={sellerUser} />
          </div>
        </div>
      </>
    );
  }
};

export default ProductPageBuyerDesktop;

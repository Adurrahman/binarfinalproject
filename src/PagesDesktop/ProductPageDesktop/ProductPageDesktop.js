import React, { useEffect, useState } from "react";
import Avatar from "../../Component/Avatar/Avatar";
import ProductImageCarousel from "../../Component/Product/ImageCarousel/ImageCarousel";
import ProductDescription from "../../Component/Product/Description/Description";
import ProductCard from "../../Component/Product/ProductCard/ProductCard";
import { useSelector } from "react-redux";
import Loader from "../../Component/Loader/Loader";

const ProductPageDesktop = () => {
  const checkLoading = useSelector((state) => state.product.loading);
  const editableProduct = useSelector((state) => state.product.newProduct);
  const [showableImage, setShowableImage] = useState([]);
  const imgData = editableProduct.getAll("images");

  useEffect(() => {
    setShowableImage(
      imgData.map((element) => {
        return URL.createObjectURL(element);
      })
    );
  }, []);

  return (
    <>
      {checkLoading ? <Loader /> : <></>}

      <div className="flex justify-center gap-8 w-full relative top-28 py-4">
        <div className="flex flex-col gap-4">
          <ProductImageCarousel
            imgList={showableImage}
            containerClass={"w-[600px] h-[436px]"}
            imageClass={"w-[600px] h-[436px] object-cover rounded-2xl"}
          />
          <ProductDescription
            desc={editableProduct.get("description")}
            containerClass={"w-[600px] p-6 rounded-2xl border"}
          />
        </div>
        <div className="w-[336px] flex flex-col gap-6">
          <ProductCard
            dataProduct={editableProduct}
            containerClass={
              "flex flex-col justify-center border w-[336px] h-[266px] rounded-2xl p-4"
            }
          />
          <Avatar />
        </div>
      </div>
    </>
  );
};

export default ProductPageDesktop;

import axios from "axios";
import React, { useState, useEffect } from "react";
import { AiOutlineEye, AiOutlineRight } from "react-icons/ai";
import { FiArrowLeft } from "react-icons/fi";
import { Link } from "react-router-dom";
import Loader from "../../Component/Loader/Loader";
import Toast from "../../Component/Toast/Toast";

const ChangePasswordDesktop = () => {
  const localUser = localStorage.getItem("user");
  const userObj = JSON.parse(localUser);
  const [inputChangePassword, setInputChangePassword] = useState({
    user_id: userObj.id,
    old_password: "",
    new_password: "",
    new_password_confirm: "",
  });
  const [passwordShow, setPasswordShow] = useState(false);

  //Validasi Password (Regex)
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [isNewPasswordValid, setIsNewPasswordValid] = useState(true);
  const [isNewPasswordConfirmValid, setIsNewPasswordConfirmValid] =
    useState(true);

  //End of Validasi Password (Regex)

  //Validasi Kesesuaian Konfirmasi Password Baru
  const [isNewPasswordMatch, setIsNewPasswordMatch] = useState(true);
  //End of Validasi Kesesauian Konfimasi Password Baru
  const [title, setTitle] = useState("");

  const [LoadingState, setLoadingState] = useState(false);
  const [message, setMessage] = useState("");

  useEffect(() => {
    document.title = "Ubah Password | RangKas";
  }, [title]);

  const tooglePasswordHandle = () => {
    setPasswordShow(!passwordShow);
  };

  const handleInputChangePassword = (e) => {
    setInputChangePassword({
      ...inputChangePassword,
      [e.target.id]: e.target.value,
    });
  };

  const handleSubmitChangePassword = (e) => {
    e.preventDefault();
    changePasswordValidation();
  };

  //Fungsi Validasi Password (Regex)
  const changePasswordValidation = () => {
    const regEx2 = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/;
    if (!regEx2.test(inputChangePassword.new_password)) {
      setIsNewPasswordValid(false);
    } else if (!regEx2.test(inputChangePassword.new_password_confirm)) {
      setIsNewPasswordConfirmValid(false);
    } else if (
      isPasswordValid === true &&
      isNewPasswordValid === true &&
      isNewPasswordConfirmValid === true
    ) {
      setIsPasswordValid(true);
      newPasswordMatchValidation();
    }
  };
  //Fungsi End of Validasi Password (Regex)

  //Funsi Validasi Kesesuaian Konfirmasi Password Baru
  const newPasswordMatchValidation = () => {
    const token = localStorage.getItem("token");
    if (
      inputChangePassword.new_password ===
      inputChangePassword.new_password_confirm
    ) {
      setIsNewPasswordMatch(true);
      setLoadingState(true);

      axios
        .post(
          `https://secondhand-serenades.herokuapp.com/api/v1/user/update_password`,
          inputChangePassword,
          {
            headers: {
              Authorization: JSON.parse(token),
              "Content-type": "application/json",
            },
          }
        )
        .then((res) => {
          setLoadingState(false);
          const responseMessage = res.data.responseMessage;
          setMessage(responseMessage);
        })
        .catch((err) => {
          setLoadingState(false);
          setMessage(err.response.data.responseMessage);
        });
    } else {
      setIsNewPasswordMatch(false);
    }
  };
  //End of Fungsi Validasi Kesesauian Konfimasi Password Baru

  useEffect(() => {
    setTimeout(() => {
      setMessage("");
    }, 3000);
  });

  if (LoadingState) {
    return <Loader />;
  } else if (!LoadingState) {
    return (
      <>
        {message === "Password lama salah" ? (
          <Toast color="bg-red-400">Password lama salah</Toast>
        ) : (
          <></>
        )}
        {message === "Ubah password berhasil" ? (
          <Toast>Ubah password berhasil</Toast>
        ) : (
          <></>
        )}

        <div className="flex justify-center">
          <div className="flex items-center fixed z-10 top-6 w-full px-4 md:hidden">
            <Link to="/profile">
              <FiArrowLeft className="text-2xl" />
            </Link>
          </div>
          <div className="hidden lg:block mt-40 pr-12  border-r-4 border-btnSecondary">
            <Link to="/profile">
              <FiArrowLeft className="text-2xl" />
            </Link>
            <p className="pt-12 font-semibold text-xl text-btnPrimary">
              Ubah Password
            </p>
          </div>
          <form
            className="pt-40 w-full md:w-1/2"
            onSubmit={handleSubmitChangePassword}
          >
            <div className="md:pl-32 pl-4 pb-4 flex flex-row items-center gap-2 text-sm">
              <Link className="cursor-pointer" to="/profile">
                <p>Pengaturan Akun</p>
              </Link>
              <AiOutlineRight />
              <p className="text-btnPrimary">Ubah Password</p>
            </div>
            <p className="font-bold text-2xl pl-4 pb-4 md:pl-32">
              Ubah Password
            </p>
            {isNewPasswordMatch ? (
              <></>
            ) : (
              <span className="px-4 md:px-32 pb-3 text-red-400 text-sm font-light">
                "New Password Confirmation is not Suitable"
              </span>
            )}
            <div className="pt-2 px-4 md:px-32">
              <label htmlFor="old_password" className="text-sm font-semibold">
                Password Lama
              </label>
              <div className="relative">
                <input
                  required
                  type={passwordShow ? "text" : "password"}
                  className=" rounded-xl
              w-full px-3 py-2 border border-gray-300
              placeholder-gray-300 text-gray-900 mb-4
              focus:outline-none focus:ring-btnPrimary
              focus:border-btnPrimary focus:z-10 sm:text-sm"
                  placeholder="Masukkan Password Lama"
                  id="old_password"
                  value={inputChangePassword.old_password}
                  onChange={handleInputChangePassword}
                />
                <AiOutlineEye
                  className="absolute inset-y-0 top-[0.3rem] right-4 text-3xl text-gray-400"
                  onClick={tooglePasswordHandle}
                />
                {isPasswordValid ? (
                  <></>
                ) : (
                  <span className="text-sm text-[#ff0000] relative -top-2">
                    "Password must be 8 character with some capital alphabet and
                    number"
                  </span>
                )}
              </div>
            </div>
            <div className="px-4 md:px-32">
              <label htmlFor="new_password" className="text-sm font-semibold">
                Password Baru
              </label>
              <div className="relative">
                <input
                  required
                  type={passwordShow ? "text" : "password"}
                  className=" rounded-xl
              w-full px-3 py-2 border border-gray-300
              placeholder-gray-300 text-gray-900 mb-4
              focus:outline-none focus:ring-btnPrimary
              focus:border-btnPrimary focus:z-10 sm:text-sm"
                  placeholder="Masukkan Password Baru"
                  id="new_password"
                  value={inputChangePassword.new_password}
                  onChange={handleInputChangePassword}
                />
                <AiOutlineEye
                  className="absolute inset-y-0 top-[0.3rem] right-4 text-3xl text-gray-400"
                  onClick={tooglePasswordHandle}
                />
                {isNewPasswordValid ? (
                  <></>
                ) : (
                  <span className="text-sm text-[#ff0000] relative -top-2">
                    "Password must be 8 character with some capital alphabet and
                    number"
                  </span>
                )}
              </div>
            </div>
            <div className="px-4 md:px-32">
              <label
                htmlFor="new_password_confirm"
                className="text-sm font-semibold"
              >
                Konfirmasi Password Baru
              </label>
              <div className="relative">
                <input
                  required
                  type={passwordShow ? "text" : "password"}
                  className=" rounded-xl
              w-full px-3 py-2 border border-gray-300
              placeholder-gray-300 text-gray-900 mb-4
              focus:outline-none focus:ring-btnPrimary
              focus:border-btnPrimary focus:z-10 sm:text-sm"
                  placeholder="Konfirmasi Password Baru"
                  id="new_password_confirm"
                  value={inputChangePassword.new_password_confirm}
                  onChange={handleInputChangePassword}
                />
                <AiOutlineEye
                  className="absolute inset-y-0 top-[0.3rem] right-4 text-3xl text-gray-400"
                  onClick={tooglePasswordHandle}
                />
                {isNewPasswordConfirmValid ? (
                  <></>
                ) : (
                  <span className="text-sm text-[#ff0000] relative -top-2">
                    "Password must be 8 character with some capital alphabet and
                    number"
                  </span>
                )}
              </div>
            </div>
            <div className="px-4 md:px-32">
              <input
                value="Change Password"
                type="submit"
                className="cursor-pointer bg-btnPrimary flex items-center justify-center text-white rounded-xl py-3 mt-10 w-full"
              />
            </div>
          </form>
        </div>
      </>
    );
  }
};

export default ChangePasswordDesktop;

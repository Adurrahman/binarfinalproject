import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import Button from "../../Component/Button/Button";
import { FiUser } from "react-icons/fi";
import { useDispatch } from "react-redux";
import { changeStatus } from "../../Store/authSlice";

function Avatar({ editable, userSeller }) {
  const user = JSON.parse(localStorage.getItem("user"));
  const dispatch = useDispatch();

  return (
    <div className="px-4 py-4 w-full h-24 rounded-xl gap-2 border shadow flex items-center">
      <div className="h-full rounded w-16 flex items-center">
        <div className="w-16 h-16">
          {userSeller ? (
            <img
              className="object-cover w-full h-full rounded"
              src={userSeller.image}
              alt="avatar"
            />
          ) : user.image ? (
            <img
              className="object-cover w-full h-full rounded"
              src={user.image}
              alt="avatar"
            />
          ) : (
            <div className="bg-bgColorSecond rounded-xl flex items-center justify-center w-16 h-16">
              <FiUser className="text-4xl text-btnPrimary" />
            </div>
          )}
        </div>
      </div>

      <div className="h-full grid content-between py-1 w-1/2 lg:w-3/4 ">
        <h1 className="truncate text-base font-medium">
          {userSeller ? userSeller.name : user.name}
        </h1>
        <h5 className="truncate text-sm font-light text-gray-400">
          {userSeller ? userSeller.city : user.city ? user.city : ""}
        </h5>
      </div>

      {editable ? (
        <div className="h-full w-full flex items-center justify-end ">
          <Link to="/edit-profile">
            <Button className="border w-16" type="outline">
              Edit
            </Button>
          </Link>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
}

export default Avatar;

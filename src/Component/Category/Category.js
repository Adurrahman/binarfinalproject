import React from "react";
import { FiSearch } from "react-icons/fi";
import { NavLink } from "react-router-dom";

function Category({
  Text = "Category",
  linkId,
  icon = <FiSearch className="mr-2 text-2xl lg:text-gray-400" />,
  isSeller = true,
}) {
  return (
    <div className="whitespace-nowrap">
      <NavLink
        to={!isSeller ? `/category/${linkId}` : `/dashboard-seller/${Text}`}
        style={({ isActive }) => {
          return {
            color: isActive ? "white" : "black",
            backgroundColor: isActive ? "#7126B5" : "#E2D4F0",
            borderRadius: "12px",
            display: "inline-flex",
            alignItems: "center",
            textAlign: "center",
            cursor: "pointer",
          };
        }}
      >
        <div className="py-3 px-4">
          <div className="flex gap-2">
            {icon}
            <p>{Text}</p>
          </div>
        </div>
      </NavLink>
    </div>
  );
}

export default Category;

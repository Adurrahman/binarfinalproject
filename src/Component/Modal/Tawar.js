import React, { useState } from "react";
import Avatar from "../Avatar/Avatar";
import MobileModal from "./MobileModal";
import { useSelector, useDispatch } from "react-redux";
import { addProductOffer } from "../../Store/offerSlice";

function Tawar({ isModalOpen, isModalClose, sellerUser }) {
  const dispatch = useDispatch();
  const localUser = localStorage.getItem("user");
  const localUserObj = JSON.parse(localUser);
  const product = useSelector((state) => state.product.product);
  const [inputOffer, setInputOffer] = useState({});

  const handleInputOffer = (e) => {
    setInputOffer({
      price: e.target.value.replace(/\D/g, ""),
      product_id: product ? product.product_id : "",
      seller_id: product ? product.user_id : "",
      buyer_id: localUserObj.id,
    });
  };

  const handleSubmitOffer = (e) => {
    e.preventDefault();
    isModalClose(false);
    dispatch(addProductOffer(inputOffer));
  };

  return (
    <MobileModal
      isModalOpen={isModalOpen}
      isModalClose={(data) => isModalClose(data)}
    >
      <div className="mt-4">
        <h1 className="font-medium text-base">Masukan Harga Tawarmu</h1>
        <p className="font-light text-gray-400 mt-2">
          Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan
          segera dihubungi penjual.
        </p>
      </div>

      <div className="mt-4">
        <Avatar editable={false} userSeller={sellerUser} />
      </div>

      <form onSubmit={handleSubmitOffer}>
        <div className="mt-4">
          <h1 className="text-base">Harga Tawar</h1>
          <input
            value={inputOffer.price}
            onChange={handleInputOffer}
            required
            type="text"
            className="border w-full rounded-lg p-3 outline-none mt-2"
            placeholder="Rp 0"
          />
        </div>

        <div className="mt-3">
          <input
            className="flex bg-btnPrimary items-center justify-center text-white rounded-xl px-8 py-3 w-full hover:bg-btnPrimaryHover hover:cursor-pointer"
            type="submit"
            value="Tawar"
          />
        </div>
      </form>
    </MobileModal>
  );
}

export default Tawar;

import React from "react";
import CustomModal from "./CustomModal";
import { FiX } from "react-icons/fi";

function MobileModal({ children, isModalOpen = false, isModalClose }) {
  return (
    <CustomModal isModalOpen={isModalOpen} onClick={() => isModalClose(false)}>
      <div
        className={`w-full min-h-40 max-h-[28rem] overflow-y-scroll scrollbar-hide fixed bg-white bottom-0 z-50 rounded-t-xl px-6 py-6 ease-in duration-300 ${
          isModalOpen ? "translate-y-0 lg:-translate-y-[100px] 2xl:-translate-y-1/2" : "translate-y-full"
        } lg:w-[26rem] lg:rounded-xl lg:inset-x-[38%]`}
      >
        {isModalOpen ? (
          <div className="relative w-full h-full">
            <FiX className="text-2xl cursor-pointer fixed text-red-700 z-[1000] top-4 right-4 hidden lg:block bg-white rounded-full" onClick={() => isModalClose(false)} />
          </div>
        ) : (
          <></>
        )}

        <div className="bg-gray-300 h-[0.2rem] w-20 rounded-full mx-auto lg:hidden mb-2"></div>
        {children}
      </div>
    </CustomModal>
  );
}

export default MobileModal;

import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { berhasilTerjual, tolakOffer } from "../../Store/offerSlice";
import MobileModal from "./MobileModal";

function PerbaruiStatus({ isModalOpen, isModalClose, dataOffer }) {
  const dispatch = useDispatch();
  const [value, setValue] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (value === "jual") {
      console.log(value);
      dispatch(berhasilTerjual(dataOffer));
    } else {
      console.log(value);
      dispatch(tolakOffer(dataOffer));
    }
  };
  return (
    <MobileModal
      isModalOpen={isModalOpen}
      isModalClose={(data) => isModalClose(data)}
    >
      <div className="mt-6">
        <h1 className="font-medium text-base">
          Perbarui status penjualan produkmu
        </h1>
      </div>

      <form className="w-full h-30" onSubmit={handleSubmit}>
        <div className="mt-6 flex items-center relative">
          <input
            type="radio"
            name="option"
            value="jual"
            required
            onChange={(e) => setValue(e.target.value)}
          />
          <label className="ml-3 flex-row">
            <p>Berhasil terjual</p>
            <p className="absolute text-gray-400">
              Kamu telah sepakat menjual produk ini kepada pembeli
            </p>
          </label>
        </div>

        <div className="mt-6 flex items-center relative top-16">
          <input
            type="radio"
            name="option"
            value="batal"
            onChange={(e) => setValue(e.target.value)}
            required
          />
          <label className="ml-3 flex-row">
            <p>Batalkan transaksi</p>
            <p className="absolute text-gray-400">
              Kamu membatalkan transaksi produk ini dengan pembeli
            </p>
          </label>
        </div>

        <div className="relative mt-40 mb-8">
          <input
            type="submit"
            value="Kirim"
            className="w-full bg-btnPrimary text-white py-2 rounded-md cursor-pointer"
          />
        </div>
      </form>
    </MobileModal>
  );
}

export default PerbaruiStatus;

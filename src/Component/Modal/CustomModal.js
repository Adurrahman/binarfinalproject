import React from "react";

function CustomModal({ isModalOpen, onClick, children }) {
  return (
    <>
      <div
        onClick={onClick}
        className={`bg-black top-0 bottom-0 right-0 left-0 bg-opacity-75 fixed z-40 ${
          isModalOpen ? "block" : "hidden"
        }`}
      ></div>
      {children}
    </>
  );
}

export default CustomModal;

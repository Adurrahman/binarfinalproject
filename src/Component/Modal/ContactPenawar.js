import React from "react";
import Button from "../Button/Button";
import MobileModal from "./MobileModal";
import { FaWhatsapp } from "react-icons/fa";

function ContactPenawar({
  isModalOpen,
  isModalClose,
  dataPembeli,
  dataProduct,
}) {
  return (
    <MobileModal
      isModalOpen={isModalOpen}
      isModalClose={(data) => isModalClose(data)}
    >
      <div className="mt-6">
        <h1 className="font-medium text-base">
          Yeay kamu berhasil mendapat harga yang sesuai
        </h1>
        <p className="font-light text-gray-400 mt-2 text-sm">
          Segera hubungi pembeli melalui whatsapp untuk transaksi selanjutnya
        </p>
      </div>

      {/* Info Penjual */}
      <div className="w-full rounded-xl p-4 mt-2 bg-gray-100">
        <h1 className="font-medium text-center mb-1">Product Match</h1>

        <div className="w-full h-16 flex gap-2 mb-5">
          <div className="h-full rounded w-16">
            <img
              className="w-full h-full object-cover rounded"
              src={dataPembeli.image}
              alt="avatar"
            />
          </div>

          <div className="px w-2/3">
            <h1 className="text-base font-medium truncate">
              {dataPembeli.name}
            </h1>
            <h5 className="text-sm font-light text-gray-400 truncate">
              {dataPembeli.city}
            </h5>
          </div>
        </div>

        {/* Info Barang */}

        <div className="w-full h-16 flex gap-2">
          <div className="h-full rounded w-16">
            <img
              className="w-full h-full object-cover rounded"
              src={dataProduct && dataProduct.product_image}
              alt="avatar"
            />
          </div>

          <div className="px w-3/4">
            <p className="text-base font-medium truncate">
              {dataProduct && dataProduct.product_name}
            </p>
            <p className="text-base font-medium truncate line-through">
              {dataProduct && dataProduct.price_origin}
            </p>
            <p className="text-base font-medium truncate">
              Ditawar Rp {dataProduct && dataProduct.price_bid}
            </p>
          </div>
        </div>
      </div>

      <div className="mt-3">
        <Button className="w-full">
          Hubungi via Whatsapp <FaWhatsapp className="ml-3 text-2xl" />
        </Button>
      </div>
    </MobileModal>
  );
}

export default ContactPenawar;

import React from "react";
import ReactDOM from "react-dom/client";
import Button from "../Button";
import { render, cleanup } from "@testing-library/react";

afterEach(cleanup);
it("renders without crashing", () => {
  const div = ReactDOM.createRoot(document.createElement("div"));
  div.render(<Button></Button>);
});

it("render children with text", () => {
  const { getByTestId } = render(<Button>click me</Button>);
  expect(getByTestId("button")).toHaveTextContent("click me");
});

it("render children without text", () => {
  const { getByTestId } = render(<Button></Button>);
  expect(getByTestId("button")).toHaveTextContent("");
});

it("render button by type primary and classname props", () => {
  const { getByTestId } = render(
    <Button type="primary" className={"h-20 w-30"}></Button>
  );
  const testClassName = "h-20 w-30";
  expect(getByTestId("button").className).toEqual(
    `cursor-pointer flex items-center justify-center bg-btnPrimary text-white rounded-md px-8 py-2 ${testClassName}`
  );
});

it("render button by type disable and classname props", () => {
  const { getByTestId } = render(
    <Button type="disable" className={"h-20 w-30"}></Button>
  );
  const testClassName = "h-20 w-30";
  expect(getByTestId("button").className).toEqual(
    `flex items-center justify-center bg-btnDisable text-white rounded-md px-8 py-2 ${testClassName}`
  );
});

it("render button by type outline and classname props", () => {
  const { getByTestId } = render(
    <Button type="outline" className={"h-20 w-30"}></Button>
  );
  const testClassName = "h-20 w-30";
  expect(getByTestId("button").className).toEqual(
    `cursor-pointer flex items-center justify-center bg-white border border-btnPrimary text-black rounded-md px-8 py-2 ${testClassName}`
  );
});

it("render button by type default and classname props", () => {
  const { getByTestId } = render(
    <Button type="" className={"h-20 w-30"}></Button>
  );
  const testClassName = "h-20 w-30";
  expect(getByTestId("button").className).toEqual(
    `cursor-pointer flex items-center justify-center bg-btnPrimary text-white rounded-md px-8 py-2 ${testClassName}`
  );
});

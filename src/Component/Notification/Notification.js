import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Navbar from "../Navbar/Navbar";
import { useDispatch, useSelector } from "react-redux";
import {
  clearDataNotification,
  getAllNotificationByUserId,
} from "../../Store/notificationSlice";
import { MutatingDots } from "react-loader-spinner";
import { BsDot } from "react-icons/bs";

const Notification = () => {
  const dispatch = useDispatch();
  const checkLoading = useSelector((state) => state.notif.loading);
  const error = useSelector((state) => state.notif.isError);
  const notification = useSelector((state) => state.notif.dataNotification);
  const localUser = localStorage.getItem("user");
  const userData = JSON.parse(localUser);
  const [title, setTitle] = useState("Notification | RangKas");

  useEffect(() => {
    document.title = title;
  }, [title]);

  const changeTitle = (event) => {
    setTitle(event.target.value);
  };

  useEffect(() => {
    dispatch(clearDataNotification());
    dispatch(getAllNotificationByUserId(userData.id));
  }, [dispatch]);

  return (
    <>
      <div className="lg:hidden">
        <Navbar Text={"Notifikasi"} />
      </div>

      <div className="w-full bg-white relative top-16 lg:w-[28rem] lg:h-80 lg:shadow-md lg:border-t lg:rounded-xl lg:absolute lg:top-9 lg:right-16 lg:overflow-y-auto custom-scroll">
        {checkLoading && !error && !notification ? (
          <div className="flex justify-center mt-20">
            <MutatingDots
              height="80"
              width="80"
              color="#7126b5"
              secondaryColor="#8a8a8a"
              ariaLabel="loading"
            />
          </div>
        ) : (
          <></>
        )}
        {notification ? (
          notification.map((item) => {
            if (!item) {
              return <></>;
            }
            return (
              <Link
                to={`/info-penawar/${item.notification_id}`}
                className="flex p-4 gap-3 items-center md:items-start"
                key={item.id}
              >
                <div className="w-20 h-20">
                  <img
                    className="w-full h-full object-cover rounded-xl"
                    src={item.product_image}
                    alt="notification_img"
                  />
                </div>

                <div className="w-4/5 flex flex-col gap-1 font-medium">
                  <div className="text-gray-400 text-sm font-light sm:flex sm:justify-between">
                    <p>{item.title}</p>
                    <div className="flex">
                      <p>{item.date}</p>
                      {item.is_read === false ? (
                        <BsDot className="text-[#fa2c5a] text-lg" />
                      ) : (
                        <></>
                      )}
                    </div>
                  </div>
                  <p className="truncate">{item.product_name}</p>
                  <p className="truncate">
                    Rp {item.price_origin.toLocaleString()}
                  </p>
                  <p className="truncate">{item.description}</p>
                </div>
              </Link>
            );
          })
        ) : (
          <></>
        )}
      </div>
    </>
  );
};

export default Notification;

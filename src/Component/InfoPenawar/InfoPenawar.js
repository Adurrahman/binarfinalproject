import React, { useEffect, useState } from "react";
import Avatar from "../Avatar/Avatar";
import ContactPenawar from "../Modal/ContactPenawar";
import PerbaruiStatus from "../Modal/PerbaruiStatus";
import { Link, useParams } from "react-router-dom";
import { FiArrowLeft } from "react-icons/fi";
import { useDispatch, useSelector } from "react-redux";
import { getNotificationById } from "../../Store/notificationSlice";
import Button from "../Button/Button";
import Loader from "../Loader/Loader";
import { FaWhatsapp } from "react-icons/fa";
import axios from "axios";
import { terimaOffer, tolakOffer } from "../../Store/offerSlice";
import imgError from "../../asset/emptyProduct.png";
const token = localStorage.getItem("token");

const InfoPenawar = () => {
  const [offerData, setOfferData] = useState({});
  const { detailNotif } = useParams();

  const dispatch = useDispatch();
  const checkLoadingNotif = useSelector(
    (state) => state.notif.loadingdetailNotification
  );
  const checkLoadingOffer = useSelector((state) => state.offer.loading);
  const statusOffer = useSelector((state) => state.offer.status);
  const error = useSelector((state) => state.notif.isError);
  const notificationDetail = useSelector(
    (state) => state.notif.detailNotification
  );
  const [loadingState, setLoadingState] = useState(false);

  useEffect(() => {
    if (notificationDetail) {
      setLoadingState(true);
      axios
        .get(
          `https://secondhand-serenades.herokuapp.com/api/v1/user/by_id?id=${notificationDetail.buyer_id}`,
          {
            headers: {
              Authorization: JSON.parse(token),
              "Content-type": "application/json",
            },
          }
        )
        .then((res) => {
          setLoadingState(false);
          const offerUser = res.data.data;
          setOfferData(offerUser);
        });
    }
  }, [notificationDetail]);
  const [title, setTitle] = useState("");

  useEffect(() => {
    document.title = notificationDetail
      ? notificationDetail.product_name + " | RangKas"
      : "Loading... ";
  }, [title, notificationDetail]);

  const changeTitle = (event) => {
    setTitle(event.target.value);
  };

  useEffect(() => {
    dispatch(getNotificationById(detailNotif));
  }, [dispatch, detailNotif]);

  useEffect(() => {
    if (statusOffer === "succeeded") {
      dispatch(getNotificationById(detailNotif));
    }
  }, [dispatch, statusOffer]);

  const [isModalOpen, setModal] = useState(false);

  function clicked(data) {
    setModal(data);
  }

  if ((checkLoadingNotif && loadingState) || checkLoadingOffer) {
    return <Loader />;
  } else if (notificationDetail && !checkLoadingNotif) {
    return (
      <>
        {checkLoadingNotif || checkLoadingOffer ? (
          <Loader />
        ) : (
          <>
            {notificationDetail && notificationDetail.offer_status === "BID" ? (
              <ContactPenawar
                dataProduct={notificationDetail}
                dataPembeli={offerData}
                isModalOpen={isModalOpen}
                isModalClose={clicked}
              />
            ) : (
              <></>
            )}

            {notificationDetail &&
            notificationDetail.offer_status === "ACCEPTED" ? (
              <PerbaruiStatus
                isModalOpen={isModalOpen}
                isModalClose={clicked}
                dataOffer={notificationDetail && notificationDetail.offer_id}
              />
            ) : (
              <></>
            )}

            <div className="flex items-center w-full lg:hidden justify-between px-6 top-8 relative">
              <Link to="/notification" className="relative z-40">
                <FiArrowLeft className="text-2xl" />
              </Link>
              <h1 className="font-medium">Info Penawar</h1>
              <div className="w-5"></div>
            </div>
            <div className="pt-16 w-full px-6 md:grid md:place-items-center relative md:top-20">
              <div className="w-full md:w-[48rem]">
                <Avatar userSeller={offerData} />
                <p className="py-4 text-sm font-medium">
                  Daftar produk yang ditawar
                </p>
                <div className="flex justify-between">
                  <div className="flex gap-4">
                    <div className="w-[76px] h-[76px] md:w-28 md:h-28">
                      <img
                        className="w-[76px] h-[76px] md:w-28 md:h-28 object-cover rounded-md"
                        src={
                          notificationDetail && notificationDetail.product_image
                        }
                        alt=""
                      />
                    </div>
                    <div>
                      <p className="text-xs text-[#8a8a8a]">
                        {notificationDetail && notificationDetail.title}
                      </p>
                      <p className="text-sm font-medium">
                        {notificationDetail && notificationDetail.product_name}
                      </p>
                      <p className="text-sm font-medium">
                        Rp{" "}
                        {notificationDetail &&
                          notificationDetail.price_origin.toLocaleString()}
                      </p>
                      <p className="text-sm font-medium">
                        Ditawar Rp{" "}
                        {notificationDetail &&
                          notificationDetail.price_bid.toLocaleString()}
                      </p>
                    </div>
                  </div>

                  <div className="flex gap-1">
                    <p className="text-xs text-[#8a8a8a]">
                      {notificationDetail && notificationDetail.date}
                    </p>
                  </div>
                </div>

                {notificationDetail &&
                notificationDetail.offer_status === "REJECTED" ? (
                  <h1 className="text-lg text-center mt-8 text-red-500 font-medium">
                    Penawaran ini sudah Kamu Tolak
                  </h1>
                ) : (
                  <></>
                )}

                {notificationDetail &&
                notificationDetail.offer_status === "COMPLETE" ? (
                  <h1 className="text-lg text-center mt-8 text-green-500 font-medium">
                    Produk Ini Sudah Kamu Jual
                  </h1>
                ) : (
                  <></>
                )}

                {notificationDetail &&
                notificationDetail.offer_status === "ACCEPTED" ? (
                  <div className="py-4 flex md:justify-end">
                    <Button
                      className="w-full rounded-2xl mr-4 md:w-auto"
                      type="outline"
                      children={<p>Status</p>}
                      onClick={() => {
                        if (notificationDetail) {
                          clicked(true);
                        }
                      }}
                    />
                    <Button
                      className="w-full rounded-2xl md:w-auto"
                      type="primary"
                      children={
                        <div className="flex items-center">
                          Hubungi di <FaWhatsapp className="ml-3" />
                        </div>
                      }
                    />
                  </div>
                ) : (
                  <></>
                )}

                {notificationDetail &&
                notificationDetail.offer_status === "BID" ? (
                  <div className="py-4 flex md:justify-end">
                    <Button
                      className="w-full rounded-2xl mr-4 md:w-auto"
                      type="outline"
                      children={<p>Tolak</p>}
                      onClick={() => {
                        notificationDetail &&
                          dispatch(tolakOffer(notificationDetail.offer_id));
                      }}
                    />
                    <Button
                      className="w-full rounded-2xl md:w-auto"
                      type="primary"
                      children={<p>Terima</p>}
                      onClick={() => {
                        if (notificationDetail) {
                          dispatch(terimaOffer(notificationDetail.offer_id));
                          clicked(true);
                        }
                      }}
                    />
                  </div>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </>
        )}
      </>
    );
  } else if (error && !notificationDetail) {
    return (
      <>
        <div className="flex items-center w-full lg:hidden justify-between px-6 top-8 relative">
          <Link to="/notification" className="relative z-40">
            <FiArrowLeft className="text-2xl" />
          </Link>
          <h1 className="font-medium">Info Penawar</h1>
          <div className="w-5"></div>
        </div>

        <div className="relative top-40 lg:top-60 mx-20">
          <img
            src={imgError}
            alt="error"
            className="w-[200px] lg:w-[300px] block mx-auto"
          />
          <h1 className="text-center text-md mt-8 text-btnPrimary">
            Maaf penawaran mu tidak ditemukan
          </h1>
        </div>
      </>
    );
  }
};

export default InfoPenawar;

import React, { useRef } from "react";
import { useDraggable } from "react-use-draggable-scroll";
import Category from "../../Component/Category/Category";
import { FiHeart, FiDollarSign, FiBox, FiSearch } from "react-icons/fi";
import { MdNavigateNext } from "react-icons/md";
import { Link, useMatch } from "react-router-dom";

const style = {
  display: "flex",
  paddingTop: "12px",
  paddingBottom: "12px",
  alignItems: "center",
  justifyContent: "space-between",
  cursor: "pointer",
};

function SellerNavCategory() {
  const ref = useRef();
  const { events } = useDraggable(ref);
  let match1 = useMatch({ path: "/dashboard-seller", end: true });
  let match2 = useMatch({ path: "/dashboard-seller/diminati", end: true });
  let match3 = useMatch({ path: "/dashboard-seller/terjual", end: true });

  return (
    <div className="px-4 w-full lg:basis-1/4 lg:px-0">
      <div
        className="w-full flex gap-4 overflow-scroll scrollbar-hide lg:block lg:gap-0 lg:border lg:p-4 lg:rounded-lg lg:shadow-md"
        {...events}
        ref={ref}
      >
        {window.innerWidth < 1024 ? (
          <>
            <Link
              to={`/dashboard-seller`}
              style={{
                color: match1 ? "white" : "black",
                backgroundColor: match1 ? "#7126B5" : "#E2D4F0",
                borderRadius: "12px",
                display: "inline-flex",
                alignItems: "center",
                textAlign: "center",
                cursor: "pointer",
              }}
            >
              <div className="py-3 px-4">
                <div className="flex gap-2">
                  <FiSearch className="mr-2 text-2xl lg:text-gray-400" />
                  <p>Semua</p>
                </div>
              </div>
            </Link>

            <Category
              Text="diminati"
              icon={<FiHeart className="mr-2 text-2xl" />}
            />
            <Category
              Text="terjual"
              icon={<FiDollarSign className="mr-2 text-2xl" />}
            />
          </>
        ) : (
          <>
            <Link
              style={style}
              to="/dashboard-seller"
              className={`border-b ${match1 ? "border-b-btnPrimary" : ""}`}
            >
              <div className="flex gap-4">
                <FiBox
                  className={`mr-2 text-2xl ${
                    match1 ? "text-btnPrimary" : "text-gray-500"
                  }`}
                />
                <p
                  className={`text-lg font-medium ${
                    match1 ? "text-btnPrimary" : "text-black"
                  }`}
                >
                  Produk
                </p>
              </div>
              <MdNavigateNext
                className={`text-2xl  ${
                  match1 ? "text-btnPrimary" : "text-gray-400"
                }`}
              />
            </Link>

            <Link
              style={style}
              to="/dashboard-seller/diminati"
              className={`border-b ${match2 ? "border-b-btnPrimary" : ""}`}
            >
              <div className="flex gap-4">
                <FiHeart
                  className={`mr-2 text-2xl ${
                    match2 ? "text-btnPrimary" : "text-gray-500"
                  }`}
                />
                <p
                  className={`text-lg font-medium ${
                    match2 ? "text-btnPrimary" : "text-black"
                  }`}
                >
                  Diminati
                </p>
              </div>
              <MdNavigateNext
                className={`text-2xl  ${
                  match2 ? "text-btnPrimary" : "text-gray-400"
                }`}
              />
            </Link>

            <Link style={style} to="/dashboard-seller/terjual">
              <div className="flex gap-4">
                <FiDollarSign
                  className={`mr-2 text-2xl ${
                    match3 ? "text-btnPrimary" : "text-gray-500"
                  }`}
                />
                <p
                  className={`text-lg font-medium ${
                    match3 ? "text-btnPrimary" : "text-black"
                  }`}
                >
                  Terjual
                </p>
              </div>
              <MdNavigateNext
                className={`text-2xl  ${
                  match3 ? "text-btnPrimary" : "text-gray-400"
                }`}
              />
            </Link>
          </>
        )}
      </div>
    </div>
  );
}

export default SellerNavCategory;

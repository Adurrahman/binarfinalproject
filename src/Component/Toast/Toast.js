import React from "react";

const Toast = ({ children, color = "bg-[#73CA5C]" }) => {
  return (
    <div
      className={`left-0 right-0 h-12 mx-8 md:mx-[500px] top-20 md:top-24 fixed z-50 rounded-xl ${color} flex justify-between items-center px-6`}
    >
      <p className="text-white">{children}</p>
    </div>
  );
};

export default Toast;

import React from 'react';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import { MutatingDots } from 'react-loader-spinner';

function Loader({ fullpage = true }) {
  return (
    <>
      {fullpage ? (
        <div className="fixed w-full h-full top-0 left-0 bg-black bg-opacity-60 z-40 grid place-items-center ">
          <MutatingDots height="100" width="100" color="#7126b5" secondaryColor="#fff" ariaLabel="loading" />
        </div>
      ) : (
        <div className="flex justify-center">
          <MutatingDots height="100" width="100" color="#7126b5" secondaryColor="#8a8a8a" ariaLabel="loading" />
        </div>
      )}
    </>
  );
}

export default Loader;

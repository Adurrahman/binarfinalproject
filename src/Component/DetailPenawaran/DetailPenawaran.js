import React from "react";
import gSeller from "../../asset/Rectangle 33.png";
import Button from "../Button/Button";

const DetailPenawaran = ({ acceptable, title, body, image, clicked }) => {
  return (
    <div>
      <div className="flex justify-between">
        <div className="flex gap-4">
          <span className="mt-1">
            <img src={gSeller} alt="" />
          </span>
          <div>
            <p className="text-xs text-[#8a8a8a]">Penawaran Produk</p>
            <p className="text-sm font-medium">Jam Tangan Casio</p>
            <p className="text-sm font-medium">Rp 250.000</p>
            <p className="text-sm font-medium">Ditawar Rp 200.000</p>
          </div>
        </div>

        <div className="flex gap-1">
          <p className="text-xs text-[#8a8a8a]">20 April, 14.04</p>
        </div>
      </div>
      {acceptable ? (
        <div className="py-4 flex md:justify-end">
          <Button
            className="w-full rounded-2xl mr-4 md:w-auto"
            type="outline"
            children={<p>Tolak</p>}
            onClick={() => clicked(false)}
          />
          <Button
            className="w-full rounded-2xl md:w-auto"
            type="primary"
            children={<p>Terima</p>}
            onClick={() => clicked(true)}
          />
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default DetailPenawaran;

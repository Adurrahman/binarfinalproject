import React from "react";

const ProductDescription = ({ containerClass, desc }) => {
  return (
    <div className={containerClass}>
      <p className="font-semibold mb-4">Deskripsi</p>
      <pre className="font-normal">{desc}</pre>
    </div>
  );
};

export default ProductDescription;

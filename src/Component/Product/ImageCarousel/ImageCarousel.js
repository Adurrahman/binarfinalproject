import React, { useRef } from "react";
import Slider from "react-slick";

import { FaChevronCircleRight, FaChevronCircleLeft } from "react-icons/fa";

const ProductImageCarousel = ({ containerClass, imageClass, imgList }) => {
  const slider = useRef(null);
  console.log(imgList);
  const settings = {
    infinite: true,
    speed: 200,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <div className={containerClass}>
      <div
        style={{ width: "inherit", height: "inherit" }}
        className="px-8 absolute justify-between items-center z-10 hidden md:inline-flex"
      >
        <button onClick={() => slider?.current?.slickPrev()}>
          <FaChevronCircleLeft size={30} color="white" />
        </button>
        <button onClick={() => slider?.current?.slickNext()}>
          <FaChevronCircleRight size={30} color="white" />
        </button>
      </div>
      <Slider ref={slider} {...settings}>
        {imgList.map((item, index) => {
          return (
            <img
              key={index}
              className={imageClass}
              src={item}
              alt="img_procuct"
            />
          );
        })}
      </Slider>
    </div>
  );
};

export default ProductImageCarousel;

import React, { useEffect } from "react";
import Button from "../../Button/Button";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { addNewProduct, changeStatus } from "../../../Store/productSlice";

const ProductCardButton = () => {
  const status = useSelector((state) => state.product.status);
  let navigate = useNavigate();
  const editableProduct = useSelector((state) => state.product.newProduct);
  const dispatch = useDispatch();

  function navigateBack() {
    navigate("/add-new-product", { replace: true });
  }

  useEffect(() => {
    if (status === "succeeded") {
      dispatch(changeStatus());
      navigate("/dashboard-seller", { replace: true });
    }
  });

  const screenWidth = window.innerWidth;
  if (screenWidth < 1024) {
    return <></>;
  } else {
    return (
      <div className="flex flex-col gap-3">
        <Button
          type="primary"
          children={<p>Terbitkan</p>}
          onClick={() => {
            dispatch(addNewProduct(editableProduct));
          }}
        />
        <Button type="outline" children={<p>Edit</p>} onClick={navigateBack} />
      </div>
    );
  }
};

const ProductCard = ({ containerClass, dataProduct }) => {
  const category = useSelector((state) => state.category.category);
  function getCategoryName() {
    let a = category.find((item) => {
      if (dataProduct.get("category_id") === item.id.toString()) {
        return item.categoryName;
      }
    });

    return a.categoryName;
  }
  return (
    <div className={containerClass}>
      <div className="py-2 flex flex-col gap-2">
        <p className="font-bold">{dataProduct.get("product_name")}</p>
        <p className="font-light text-gray-400">{getCategoryName()}</p>
      </div>
      <p className="font-medium md:py-4">{dataProduct.get("price")}</p>
      <ProductCardButton />
    </div>
  );
};

export default ProductCard;

import React from "react";
import Cardku from "../../Component/Card/CustomCard";
import { Link } from "react-router-dom";
import { FiPlus } from "react-icons/fi";
import emptyProduct from "../../asset/emptyProduct.png";
import { useSelector } from "react-redux";

function ContainerCard({ isSeller = true, data }) {
  const token = localStorage.getItem("token");
  const dataUser = localStorage.getItem("user");
  const idUser = dataUser ? JSON.parse(dataUser).id : null;
  const checkLoadingProduct = useSelector((state) => state.product.loading);

  return (
    <>
      <div
        className={`px-4 bg-white w-full pb-40 grid grid-cols-2 gap-y-5 gap-x-3 ${
          isSeller
            ? `lg:grid-cols-3 lg:basis-3/4 mt-10`
            : `lg:grid-cols-5 lg:relative lg:top-48 lg:px-32`
        } lg:mt-0`}
      >
        {isSeller ? (
          <Link
            to="/add-new-product"
            className="border-dashed border-2 text-gray-400 flex justify-center items-center h-48 md:h-64"
          >
            <div className="w-full text-center">
              <FiPlus className="text-4xl mx-auto" />
              <h3>Tambah Produk</h3>
            </div>
          </Link>
        ) : (
          <></>
        )}

        {!checkLoadingProduct && !data && !isSeller ? (
          <>
            <div className="md:w-full absolute top-[28rem] md:top-16 h-[20rem] left-0 right-0">
              <img src={emptyProduct} className="block mx-auto" />
              <div className="w-80 h-20 mx-auto mt-3">
                <p className="text-center">
                  Kategori yang kamu cari belum ada nih
                </p>
              </div>
            </div>
          </>
        ) : (
          data &&
          data.map((item) => {
            return (
              <Link
                key={item.product_id}
                to={
                  token
                    ? isSeller
                      ? `/edit-product/${item.product_id}`
                      : `/detail-product/${item.product_id}`
                    : `/login`
                }
              >
                <Cardku Product={item} id={item.product_id} />
              </Link>
            );
          })
        )}
      </div>
    </>
  );
}

export default ContainerCard;

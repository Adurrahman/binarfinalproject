import React from "react";
import { Outlet } from "react-router-dom";
import NavbarDesktop from "../Navbar/NavbarDesktop";

function LayoutDesktop() {
  return (
    <>
      <NavbarDesktop />
      <Outlet />
    </>
  );
}

export default LayoutDesktop;

import React, { useCallback, useEffect, useState } from "react";
import { FiSearch, FiMenu, FiX, FiLogIn } from "react-icons/fi";
import Button from "../Button/Button";
import { Link } from "react-router-dom";
import CustomModal from "../Modal/CustomModal";
import { debounce } from "lodash";
import {
  getAllProducts,
  getAllProductsPage,
  getProductByName,
} from "../../Store/productSlice";
import { useDispatch } from "react-redux";

function Navbar({ Text }) {
  const user = localStorage.getItem("user");
  const [search, setSearch] = useState("");
  let token = localStorage.getItem("token");
  const [isSideOpen, setIsOpen] = useState(false);
  const [isLogin, setIsLogin] = useState(false);
  const userLocal = localStorage.getItem("user");
  const userData = JSON.parse(userLocal);

  const dispatch = useDispatch();

  const findName = useCallback(
    debounce((text) => {
      if (!text) {
        dispatch(getAllProducts);
      } else {
        dispatch(getProductByName(text));
      }
    }, 1000),
    []
  );

  const handleSearch = (text) => {
    setSearch(text);
    if (!text) {
      dispatch(getAllProductsPage(1));
    }
  };

  useEffect(() => {
    findName(search);
  }, [search]);

  useEffect(() => {
    if (user && token) {
      setIsLogin(true);
    }
  }, [user, token]);
  return (
    <>
      {/* Modal Sidebar */}
      <CustomModal isModalOpen={isSideOpen} onClick={() => setIsOpen(false)}>
        <div
          className={`w-1/2 fixed z-50 bg-white px-4 ease-in duration-300 h-screen top-0 ${
            isSideOpen ? "translate-x-0" : "-translate-x-full"
          } pt-8`}
        >
          <div className="flex justify-between">
            <Link to="/" className="font-bold font-body text-btnPrimary">
              Rangkas
            </Link>
            <FiX
              className="cursor-pointer active:text-btnPrimary text-2xl"
              onClick={() => {
                setIsOpen(false);
              }}
            />
          </div>

          {isLogin ? (
            <div className="mt-3 flex flex-col">
              <Link
                to="/notification"
                className="font-body hover:bg-bgColorSecond cursor-pointer py-2 rounded pl-2"
              >
                Notifikasi
              </Link>
              <Link
                to={
                  userData.is_valid_user ? "/dashboard-seller" : "/edit-profile"
                }
                className="my-3 font-body hover:bg-bgColorSecond cursor-pointer py-2 rounded pl-2"
              >
                Daftar Jual
              </Link>
              <Link
                to="/profile"
                className="font-body hover:bg-bgColorSecond cursor-pointer py-2 rounded pl-2"
              >
                Akun Saya
              </Link>
            </div>
          ) : (
            <Link to="login">
              <Button className="mt-3 px-4">
                <FiLogIn className="mr-3 text-xl" /> Masuk
              </Button>
            </Link>
          )}
        </div>
      </CustomModal>
      {/* Modal Sidebar */}

      {/* Start of Navbar */}
      <div
        className={`flex items-center ${
          Text ? "absolute" : "fixed"
        } z-10 top-3 w-full px-4`}
      >
        <div
          className="w-12 h-12 cursor-pointer bg-white rounded-md p-3 mr-3"
          onClick={() => {
            setIsOpen(true);
          }}
        >
          <FiMenu className="text-2xl" />
        </div>

        <div className="relative w-full flex">
          {Text ? (
            <h1 className="font-bold text-xl">{Text}</h1>
          ) : (
            <>
              <input
                type="text"
                className="w-full py-3 rounded-lg px-3 outline-none"
                placeholder="Cari di sini ..."
                value={search}
                onChange={(event) => handleSearch(event.target.value)}
              />
              <div className="absolute top-3 right-4">
                <FiSearch className="text-gray-400 text-2xl" />
              </div>{" "}
            </>
          )}
        </div>
      </div>
      {/* End of Navbar */}
    </>
  );
}

export default Navbar;

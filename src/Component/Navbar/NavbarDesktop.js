import React, { useCallback, useEffect, useState } from "react";
import Button from "../Button/Button";
import { FiSearch, FiLogIn } from "react-icons/fi";
import Logo from "../../asset/rangkas-logo.png";
import { Link, useLocation } from "react-router-dom";
import IconNavbar from "../IconNavbar/IconNavbar";
import { debounce } from "lodash";
import { useDispatch } from "react-redux";
import {
  getAllProducts,
  getAllProductsPage,
  getProductByName,
} from "../../Store/productSlice";

function NavbarDesktop() {
  const user = localStorage.getItem("user");
  let token = localStorage.getItem("token");
  const [isLogin, setIsLogin] = useState(false);
  const [search, setSearch] = useState("");
  const { pathname } = useLocation();
  let location = pathname.split("/");
  let currentLocation = location.pop();

  const dispatch = useDispatch();

  const findName = useCallback(
    debounce((text) => {
      if (!text) {
        dispatch(getAllProducts);
      } else {
        dispatch(getProductByName(text));
      }
    }, 1000),
    []
  );

  const handleSearch = (text) => {
    setSearch(text);
    if (!text) {
      dispatch(getAllProductsPage(1));
    }
  };

  useEffect(() => {
    findName(search);
  }, [search]);

  useEffect(() => {
    if (user && token) {
      setIsLogin(true);
    }
  }, [user, token]);

  switch (currentLocation) {
    case "add-new-product":
      return (
        <div className="flex z-30 items-center fixed bg-white shadow-md h-20 w-full px-32 ">
          <Link to="/">
            <div className="mr-3">
              <img src={Logo} alt="logo" />
            </div>
          </Link>
        </div>
      );

    case "edit-profile":
      return (
        <div className="flex z-30 items-center fixed bg-white shadow-md h-20 w-full px-32 ">
          <Link to="/">
            <div className="mr-3">
              <img src={Logo} alt="logo" />
            </div>
          </Link>

          <div className="w-full">
            <h3 className="text-md font-medium text-center">
              Lengkapi Info Akun
            </h3>
          </div>
        </div>
      );

    case "infopenawar":
      return (
        <div className="flex z-30 items-center fixed bg-white shadow-md h-20 w-full px-32 ">
          <Link to="/">
            <div className="mr-3">
              <img src={Logo} alt="logo" />
            </div>
          </Link>

          <div className="w-full">
            <h3 className="text-md font-medium text-center">Info Penawar</h3>
          </div>
        </div>
      );

    default:
      return (
        <div className="flex z-30 items-center fixed bg-white shadow-md h-20 w-full px-32 ">
          <Link to="/">
            <div className="mr-3">
              <img src={Logo} alt="logo" />
            </div>
          </Link>

          <div className="relative w-full flex basis-1/3">
            <input
              type="text"
              className="w-full py-3 rounded-lg px-3 outline-none lg:bg-[#eee]"
              placeholder="Cari di sini ..."
              value={search}
              onChange={(event) => handleSearch(event.target.value)}
            />
            <div className="absolute top-3 right-4">
              <FiSearch className="text-gray-400 text-2xl" />
            </div>
          </div>

          <div className="grow ">
            {isLogin ? (
              <IconNavbar />
            ) : (
              <Link to="/login">
                <Button className="float-right px-4">
                  <FiLogIn className="mr-3 text-xl" />
                  Masuk
                </Button>
              </Link>
            )}
          </div>
        </div>
      );
  }
}

export default NavbarDesktop;

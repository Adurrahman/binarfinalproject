import React from "react";
import { useSelector } from "react-redux";
import { Card, CardBody, CardTitle, CardText } from "reactstrap";

function Cardku({ Product }) {
  const category = useSelector((state) => state.category.category);

  function getCategoryName() {
    let a =
      category &&
      category.find((item) => {
        if (Product.category_id.toString() === item.id.toString()) {
          return item;
        }
      });
    return a ? a.categoryName : "Other";
  }

  return (
    <div className="card">
      <Card className="w-full shadow-md cursor-pointer p-2 rounded hover:shadow-xl border h-48 md:h-64">
        <CardBody>
          <img
            src={Product.images[0].url}
            className="w-full h-24 md:h-40 object-cover"
          />
          <CardTitle tag="h5" className="text-sm truncate">
            {Product.product_name}
          </CardTitle>
          <CardText className="text-xs text-[#8a8a8a] mt-1 mb-2">
            {getCategoryName()}
          </CardText>
          <CardText className="text-sm">
            Rp {Product.price.toLocaleString()}
          </CardText>
        </CardBody>
      </Card>
    </div>
  );
}

export default Cardku;

import React from 'react';
import NavbarDesktop from '../Navbar/NavbarDesktop';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Slider from 'react-slick';
import bgRamadhan from '../../asset/bgImage.png';
import gift from '../../asset/gift.png';

function HeaderDesktop() {
  const settings = {
    className: 'center',
    centerPadding: '280px',
    draggable: true,
    centerMode: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    adaptiveHeight: true,
  };
  return (
    <>
      <NavbarDesktop />
      <div className="relative top-28">
        <Slider {...settings}>
          <div className="relative rounded-lg h-72 bg-white flex">
            <div className="absolute rounded-lg w-full h-full bg-gradient-to-r from-bgColorMain via-[#FFE9CA]">
              <div className="w-80 my-14 ml-20">
                <h1 className="text-3xl font-bold">Bulan Ramadhan Banyak diskon!</h1>
                <h5 className="mt-5 mb-3 text-lg">Diskon Hingga</h5>
                <h2 className="text-red-500 font-medium text-xl">60%</h2>
              </div>
              <img className="absolute left-1/2 top-20" src={gift} alt="..." />
            </div>

            <div className="flex flex-row-reverse w-full h-full rounded-lg">
              <div className="basis-1/2 h-full">
                <img className="rounded-lg w-full h-full object-cover" src={bgRamadhan} alt="bgImage" />
              </div>
            </div>
          </div>

          <div className="relative rounded-lg h-72 bg-[#E2D4F0]"></div>
          <div className="relative rounded-lg h-72 bg-[#B6D4A8]"></div>
        </Slider>
      </div>
    </>
  );
}

export default HeaderDesktop;

import React from "react";
import Navbar from "../Navbar/Navbar";
import gift from "../../asset/gift.png";

function Header() {
  return (
    <>
      <Navbar />

      <div className="w-full h-[30rem] relative">
        <div className="bg-gradient-to-b from-bgColorMain via-[#FFE9CA] w-full h-full px-4 pt-20 flex justify-between">
          <div className="w-1/2">
            <h1 className="text-lg font-bold">Bulan Ramadhan Banyak diskon!</h1>
            <h5 className="mt-5 mb-3">Diskon Hingga</h5>
            <h2 className="text-red-500 font-medium text-xl">60%</h2>
          </div>
          <div>
            <img
              className="w-32 absolute right-7 top-24"
              src={gift}
              alt="..."
            />
          </div>
        </div>
      </div>
    </>
  );
}

export default Header;

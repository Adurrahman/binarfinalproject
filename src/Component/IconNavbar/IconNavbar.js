import React, { useState } from "react";
import { FiUser, FiList, FiBell } from "react-icons/fi";
import { Link, useMatch } from "react-router-dom";
import Notification from "../Notification/Notification";

function IconNavbar() {
  let matchDashboard = useMatch({ path: "/dashboard-seller", end: true });
  let matchProfile = useMatch({ path: "/profile", end: true });
  const [isActive, setIsActive] = useState(false);

  window.addEventListener("click", (e) => {
    if (e.target.id !== "bell") {
      setIsActive(false);
    }
  });
  return (
    <>
      <div className="flex w-40 float-right justify-between relative">
        <Link to="/dashboard-seller">
          <FiList
            className={`cursor-pointer hover:text-btnPrimary hover:bg-bgColorSecond rounded p-1 w-8 h-8 ${
              matchDashboard ? "text-btnPrimary bg-bgColorSecond" : ""
            }`}
          />
        </Link>

        <FiBell
          data-testid="fibell"
          id="bell"
          className="cursor-pointer hover:text-btnPrimary hover:bg-bgColorSecond rounded p-1 w-8 h-8"
          onClick={() => {
            if (isActive) {
              setIsActive(false);
            } else {
              setIsActive(true);
            }
          }}
        />

        <Link to="/profile">
          <FiUser
            className={`cursor-pointer hover:text-btnPrimary hover:bg-bgColorSecond rounded p-1 w-8 h-8 ${
              matchProfile ? "text-btnPrimary bg-bgColorSecond" : ""
            }`}
          />
        </Link>

        {isActive ? <Notification /> : <></>}
      </div>
    </>
  );
}

export default IconNavbar;

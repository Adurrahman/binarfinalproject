import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
const token = localStorage.getItem("token");

export const userLogin = createAsyncThunk(
  "auth/userLogin",
  async (payload, { rejectWithValue }) => {
    try {
      const response = await axios.post(
        "https://secondhand-serenades.herokuapp.com/api/v1/user/login",
        payload
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const userRegister = createAsyncThunk(
  "auth/userRegister",
  async (payload, { rejectWithValue }) => {
    try {
      const response = await axios.post(
        "https://secondhand-serenades.herokuapp.com/api/v1/user/register",
        payload
      );
      return response.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const userEdit = createAsyncThunk(
  "auth/userEdit",
  async (payload, { rejectWithValue }) => {
    try {
      const response = await axios.post(
        "https://secondhand-serenades.herokuapp.com/api/v1/user/completing_data",
        payload,
        {
          headers: {
            Authorization: JSON.parse(token),
            "Content-type": "application/json",
          },
        }
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const updatePassword = createAsyncThunk(
  "auth/changePassword",
  async (payload, { rejectWithValue }) => {
    try {
      const response = await axios.post(
        "https://secondhand-serenades.herokuapp.com/api/v1/user/update_password",
        payload,
        {
          headers: {
            Authorization: JSON.parse(token),
            "Content-type": "application/json",
          },
        }
      );
      return response.responseMessage;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getUserById = createAsyncThunk(
  "auth/getUserById",
  async (seller_id, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        `https://secondhand-serenades.herokuapp.com/api/v1/user/by_id?id=${seller_id}`,
        {
          headers: {
            Authorization: JSON.parse(token),
          },
        }
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

const initialState = {
  user: null,
  sellerUser: null,
  loading: false,
  isError: null,
  status: "idle",
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    cleanError: (state) => {
      state.isError = null;
    },
    changeStatusUser: (state) => {
      state.status = "idle";
    },
    resetReducerAuth(state, action) {
      return initialState;
    },
  },
  extraReducers: {
    // Handle Login
    [userLogin.pending]: (state) => {
      state.loading = true;
      state.user = null;
      state.isError = null;
    },
    [userLogin.fulfilled]: (state, action) => {
      state.user = action.payload;
      localStorage.setItem("user", JSON.stringify(action.payload));
      localStorage.setItem("token", JSON.stringify(action.payload.token));
      state.isError = null;
      state.loading = false;
    },
    [userLogin.rejected]: (state, action) => {
      state.loading = false;
      state.user = null;
      state.isError = action.payload;
    },
    // End Of Handle Login

    // Handle Register
    [userRegister.pending]: (state) => {
      state.loading = true;
      state.user = null;
      state.isError = null;
    },
    [userRegister.fulfilled]: (state, action) => {
      state.user = action.payload;
      state.isError = null;
      state.loading = false;
    },
    [userRegister.rejected]: (state, action) => {
      state.loading = false;
      state.user = null;
      state.isError = action.payload;
    },
    // End Of Handle Register

    // Handle Edit Profile
    [userEdit.pending]: (state) => {
      state.loading = true;
      state.user = null;
      state.status = "loading";
      state.isError = null;
    },
    [userEdit.fulfilled]: (state, action) => {
      state.user = action.payload;
      localStorage.setItem("user", JSON.stringify(action.payload));
      state.isError = null;
      state.status = "succeeded";
      state.loading = false;
    },
    [userEdit.rejected]: (state, action) => {
      state.loading = false;
      state.status = "failed";
      state.user = null;
      state.isError = action.payload;
    },
    // End Of Edit Profile

    // Handle get user by id
    [getUserById.pending]: (state) => {
      state.loading = true;
      state.sellerUser = null;
      state.isError = null;
    },
    [getUserById.fulfilled]: (state, action) => {
      state.sellerUser = action.payload;
      state.isError = null;
      state.loading = false;
    },
    [getUserById.rejected]: (state, action) => {
      state.loading = false;
      state.sellerUser = null;
      state.isError = action.payload;
    },
    // End Of get user by id

    // Handle Update Password
    [updatePassword.pending]: (state) => {
      state.loading = true;
      state.user = null;
      state.isError = null;
    },
    [updatePassword.fulfilled]: (state, action) => {
      state.user = action.payload;
      state.isError = null;
      state.loading = false;
    },
    [updatePassword.rejected]: (state, action) => {
      state.loading = false;
      state.user = null;
      state.isError = action.payload;
    },
    // End of Update Password
  },
});

export const { cleanError, changeStatusUser, resetReducerAuth } =
  authSlice.actions;

export default authSlice.reducer;

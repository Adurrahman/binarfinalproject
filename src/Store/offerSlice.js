import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
const token = localStorage.getItem("token");

export const getOfferSeller = createAsyncThunk(
  "offer/getOfferSeller",
  async (_, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        `https://secondhand-serenades.herokuapp.com/api/v1/product-offer/show-all-product-offer/seller?seller_id=1`,
        {
          headers: {
            Authorization: JSON.parse(token),
          },
        }
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getOfferBuyer = createAsyncThunk(
  "offer/getOfferBuyer",
  async (_, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        `https://secondhand-serenades.herokuapp.com/api/v1/product-offer/show-all-product-offer/buyer?buyer_id=2`,
        {
          headers: {
            Authorization: JSON.parse(token),
          },
        }
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const addProductOffer = createAsyncThunk(
  "offer/addPorductOffer",
  async (payload, { rejectWithValue }) => {
    try {
      const response = await axios.post(
        `https://secondhand-serenades.herokuapp.com/api/v1/product-offer/add-product-offer`,
        payload,
        {
          headers: {
            Authorization: JSON.parse(token),
          },
        }
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const tolakOffer = createAsyncThunk(
  "offer/tolakOffer",
  async (id, { rejectWithValue }) => {
    let payload = { offer_id: id };
    try {
      const response = await axios.post(
        `https://secondhand-serenades.herokuapp.com/api/v1/product-offer/batalkan-transaksi`,
        payload,
        {
          headers: {
            Authorization: JSON.parse(token),
          },
        }
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const terimaOffer = createAsyncThunk(
  "offer/terimaOffer",
  async (id, { rejectWithValue }) => {
    let payload = { offer_id: id };
    try {
      const response = await axios.post(
        `https://secondhand-serenades.herokuapp.com/api/v1/product-offer/product-terima-tawaran`,
        payload,
        {
          headers: {
            Authorization: JSON.parse(token),
          },
        }
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const berhasilTerjual = createAsyncThunk(
  "offer/berhasilTerjual",
  async (id, { rejectWithValue }) => {
    let payload = { offer_id: id };
    try {
      const response = await axios.post(
        `https://secondhand-serenades.herokuapp.com/api/v1/product-offer/product-berhasil-terjual`,
        payload,
        {
          headers: {
            Authorization: JSON.parse(token),
          },
        }
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

const initialState = {
  offer: null,
  loading: false,
  status: "idle",
  isError: null,
};

export const offerSlice = createSlice({
  name: "offer",
  initialState,
  reducers: {
    changeStatusOffer: (state) => {
      state.status = "idle";
    },
    resetReducerOffer(state, action) {
      return initialState;
    },
  },
  extraReducers: {
    //handle Add Product Offer
    [addProductOffer.pending]: (state) => {
      state.status = "loading";
      state.loading = true;
      state.offer = null;
      state.isError = null;
    },
    [addProductOffer.fulfilled]: (state, action) => {
      state.offer = action.payload;
      state.status = "succeeded";
      state.isError = null;
      state.loading = false;
    },
    [addProductOffer.rejected]: (state, action) => {
      state.loading = false;
      state.status = "failed";
      state.offer = null;
      state.isError = action.payload;
    },
    //end of Add Product Offer

    //handle Tolak Offer
    [tolakOffer.pending]: (state) => {
      state.loading = true;
      state.status = "loading";
      state.isError = null;
    },
    [tolakOffer.fulfilled]: (state) => {
      state.isError = null;
      state.status = "succeeded";
      state.loading = false;
    },
    [tolakOffer.rejected]: (state, action) => {
      state.status = "failed";
      state.loading = false;
      state.isError = action.payload;
    },
    //end of Tolak Offer

    //handle terima Offer
    [terimaOffer.pending]: (state) => {
      state.loading = true;
      state.status = "loading";
      state.isError = null;
    },
    [terimaOffer.fulfilled]: (state) => {
      state.isError = null;
      state.status = "succeeded";
      state.loading = false;
    },
    [terimaOffer.rejected]: (state, action) => {
      state.status = "failed";
      state.loading = false;
      state.isError = action.payload;
    },
    //end of terima Offer

    //handle terjual
    [berhasilTerjual.pending]: (state) => {
      state.loading = true;
      state.status = "loading";
      state.isError = null;
    },
    [berhasilTerjual.fulfilled]: (state) => {
      state.isError = null;
      state.status = "succeeded";
      state.loading = false;
    },
    [berhasilTerjual.rejected]: (state, action) => {
      state.status = "failed";
      state.loading = false;
      state.isError = action.payload;
    },
    //end of terjual
  },
});

export const { changeStatusOffer, resetReducerOffer } = offerSlice.actions;
export default offerSlice.reducer;

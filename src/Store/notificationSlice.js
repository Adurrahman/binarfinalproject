import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
const token = localStorage.getItem("token");

export const getAllNotificationByUserId = createAsyncThunk(
  "notification/getAllNotificationByUserId",
  async (payload, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        `https://secondhand-serenades.herokuapp.com/api/v1/notification/show-notification-by-user-id/${payload}`,
        {
          headers: {
            Authorization: JSON.parse(token),
          },
        }
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getNotificationById = createAsyncThunk(
  "notification/getNotificationById",
  async (notification_id, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        `https://secondhand-serenades.herokuapp.com/api/v1/notification/show-notification-by-id/${notification_id}`,
        {
          headers: {
            Authorization: JSON.parse(token),
          },
        }
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

const initialState = {
  dataNotification: null,
  detailNotification: null,
  loading: false,
  isError: null,
  status: "idle",
};

export const notificationSlice = createSlice({
  name: "notification",
  initialState,
  reducers: {
    resetReducerNotification(state, action) {
      return initialState;
    },
    clearDataNotification(state) {
      state.dataNotification = null;
    },
  },
  extraReducers: {
    // Handle Get All notification by userid
    [getAllNotificationByUserId.pending]: (state) => {
      state.loading = true;
      state.dataNotification = null;
      state.status = "loading";
      state.isError = null;
    },
    [getAllNotificationByUserId.fulfilled]: (state, action) => {
      state.dataNotification = action.payload;
      state.isError = null;
      state.status = "succeeded";
      state.loading = false;
    },
    [getAllNotificationByUserId.rejected]: (state, action) => {
      state.loading = false;
      state.status = "failed";
      state.dataNotification = null;
      state.isError = action.payload;
    },
    // End of Handle Get All notification by userid

    // Handle Get detail notification by id
    [getNotificationById.pending]: (state) => {
      state.loading = true;
      state.detailNotification = null;
      state.status = "loading";
      state.isError = null;
    },
    [getNotificationById.fulfilled]: (state, action) => {
      state.detailNotification = action.payload;
      state.isError = null;
      state.status = "succeeded";
      state.loading = false;
    },
    [getNotificationById.rejected]: (state, action) => {
      state.loading = false;
      state.status = "failed";
      state.detailNotification = null;
      state.isError = action.payload;
    },
    // End of Handle Get detail notification by id
  },
});
export const { resetReducerNotification, clearDataNotification } =
  notificationSlice.actions;

export default notificationSlice.reducer;

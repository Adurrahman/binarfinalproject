import { combineReducers, configureStore } from "@reduxjs/toolkit";
import authSlice from "./authSlice";
import categorySlice from "./categorySlice";
import notificationSlice from "./notificationSlice";
import offerSlice from "./offerSlice";
import productSlice from "./productSlice";

// Create the root reducer separately so we can extract the RootState type
const rootReducer = combineReducers({
  auth: authSlice,
  category: categorySlice,
  notif: notificationSlice,
  product: productSlice,
  offer: offerSlice,
});

export const setupStore = (preloadedState) => {
  return configureStore({
    reducer: rootReducer,
    preloadedState,
  });
};

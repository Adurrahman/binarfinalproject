import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
const dataUser = localStorage.getItem("user");
const idUser = dataUser ? JSON.parse(dataUser).id : null;

export const addNewProduct = createAsyncThunk(
  "product/addNewProduct",
  async (payload, { rejectWithValue }) => {
    try {
      const response = await axios.post(
        "https://secondhand-serenades.herokuapp.com/api/v1/product/add-product",
        payload
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getAllProducts = createAsyncThunk(
  "product/getAllProducts",
  async (_, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        "https://secondhand-serenades.herokuapp.com/api/v1/product/show-all-product"
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getAllProductsPage = createAsyncThunk(
  "product/getAllProducts",
  async (page, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        `https://secondhand-serenades.herokuapp.com/api/v1/product/show-all-product-limit?${
          idUser ? `user_id=${idUser}` : ""
        }&page=${page}&data_size=10`
      );
      return response.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getAllProductsSeller = createAsyncThunk(
  "products/getAllProductsSeller",
  async (_, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        `https://secondhand-serenades.herokuapp.com/api/v1/product/show-product-status-available/id?user_id=${idUser}`
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getProductByID = createAsyncThunk(
  "product/getProductById",
  async (idProduct, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        `https://secondhand-serenades.herokuapp.com/api/v1/product/show-product-by-id/${idProduct}`
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getProductByCategory = createAsyncThunk(
  "product/getProductByCategory",
  async (idCategory, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        `https://secondhand-serenades.herokuapp.com/api/v1/product/show-product-by-category/${idCategory}`
      );
      return response.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getAllProductsTerjual = createAsyncThunk(
  "products/getAllProductsSeller",
  async (_, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        `https://secondhand-serenades.herokuapp.com/api/v1/product/show-product-status-sold/id?user_id=${idUser}`
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getAllProductsTertawar = createAsyncThunk(
  "products/getAllProductsSeller",
  async (_, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        `https://secondhand-serenades.herokuapp.com/api/v1/product/show-product-status-bid/id?user_id=${idUser}`
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getProductByName = createAsyncThunk(
  "products/getProductByName",
  async (productName, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        `https://secondhand-serenades.herokuapp.com/api/v1/product/show-product-by-name/${productName}`
      );
      return response.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const deleteProduct = createAsyncThunk(
  "products/deleteProduct",
  async (productID, { rejectWithValue }) => {
    try {
      const response = await axios.delete(
        `https://secondhand-serenades.herokuapp.com/api/v1/product/remove/${productID}`
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const updateProduct = createAsyncThunk(
  "products/updateProduct",
  async (dataForm, { rejectWithValue }) => {
    try {
      const response = await axios.post(
        `https://secondhand-serenades.herokuapp.com/api/v1/product/update-product`,
        dataForm
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

const initialState = {
  newProduct: null,
  editProduct: null,
  product: null,
  status: "idle",
  products: null,
  productsSeller: null,
  loading: false,
  isError: null,
};

export const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    previewProduct: (state, action) => {
      state.newProduct = action.payload;
    },
    previewEditProduct: (state, action) => {
      state.editProduct = action.payload;
    },
    delPreviewEditProduct: (state) => {
      state.editProduct = null;
    },
    changeStatus: (state) => {
      state.status = "idle";
    },
    resetReducerProduct(state) {
      state = initialState;
    },
    clearProducts(state) {
      state.products = null;
    },
  },
  extraReducers: {
    // Handle add new product
    [addNewProduct.pending]: (state) => {
      state.status = "loading";
      state.loading = true;
      state.product = null;
      state.isError = null;
    },
    [addNewProduct.fulfilled]: (state, action) => {
      state.product = action.payload;
      state.status = "succeeded";
      state.isError = null;
      state.loading = false;
    },
    [addNewProduct.rejected]: (state, action) => {
      state.loading = false;
      state.status = "failed";
      state.product = null;
      state.isError = action.payload;
    },
    // End of Handle add new product

    // Handle Get All Products
    [getAllProducts.pending]: (state) => {
      state.loading = true;
      state.products = null;
      state.isError = null;
    },
    [getAllProducts.fulfilled]: (state, action) => {
      state.products = action.payload;
      state.isError = null;
      state.loading = false;
    },
    [getAllProducts.rejected]: (state, action) => {
      state.loading = false;
      state.products = null;
      state.isError = action.payload;
    },
    // End of Get all products

    // Handle Get All Products per Page
    [getAllProductsPage.pending]: (state) => {
      state.loading = true;
      state.products = null;
      state.isError = null;
    },
    [getAllProductsPage.fulfilled]: (state, action) => {
      state.products = action.payload;
      state.isError = null;
      state.loading = false;
    },
    [getAllProductsPage.rejected]: (state, action) => {
      state.loading = false;
      state.products = null;
      state.isError = action.payload;
    },
    // End of Get all products per Page

    // Handle Get All Products Seller
    [getAllProductsSeller.pending]: (state) => {
      state.loading = true;
      state.productsSeller = null;
      state.isError = null;
    },
    [getAllProductsSeller.fulfilled]: (state, action) => {
      state.productsSeller = action.payload;
      state.isError = null;
      state.loading = false;
    },
    [getAllProductsSeller.rejected]: (state, action) => {
      state.loading = false;
      state.productsSeller = null;
      state.isError = action.payload;
    },
    // End of Get all product seller

    // Handel Get Product by ID
    [getProductByID.pending]: (state) => {
      state.loading = true;
      state.product = null;
      state.isError = null;
    },
    [getProductByID.fulfilled]: (state, action) => {
      state.product = action.payload;
      state.isError = null;
      state.loading = false;
    },
    [getProductByID.rejected]: (state, action) => {
      state.loading = false;
      state.product = null;
      state.isError = action.payload;
    },
    //End of Get Product by ID

    // Handel Get Product by CategoryID
    [getProductByCategory.pending]: (state) => {
      state.loading = true;
      state.products = null;
      state.isError = null;
    },
    [getProductByCategory.fulfilled]: (state, action) => {
      state.products = action.payload;
      state.isError = null;
      state.loading = false;
    },
    [getProductByCategory.rejected]: (state, action) => {
      state.loading = false;
      state.products = null;
      state.isError = action.payload;
    },
    //End of Get Product by ID

    // Handel Get Product by Name
    [getProductByName.pending]: (state) => {
      state.loading = true;
      state.products = null;
      state.isError = null;
    },
    [getProductByName.fulfilled]: (state, action) => {
      state.products = action.payload;
      state.isError = null;
      state.loading = false;
    },
    [getProductByName.rejected]: (state, action) => {
      state.loading = false;
      state.products = null;
      state.isError = action.payload;
    },
    //End of Get Product by Name

    // Handel Remove Product
    [deleteProduct.pending]: (state) => {
      state.loading = true;
      state.isError = null;
    },
    [deleteProduct.fulfilled]: (state) => {
      state.isError = null;
      state.loading = false;
    },
    [deleteProduct.rejected]: (state, action) => {
      state.loading = false;
      state.isError = action.payload;
    },
    //End of Remove Product

    // Handel Remove Product
    [updateProduct.pending]: (state) => {
      state.status = "loading";
      state.loading = true;
      state.isError = null;
    },
    [updateProduct.fulfilled]: (state) => {
      state.status = "succeeded";
      state.isError = null;
      state.editProduct = null;
      state.loading = false;
    },
    [updateProduct.rejected]: (state, action) => {
      state.loading = false;
      state.status = "failed";
      state.isError = action.payload;
    },
    //End of Remove Product
  },
});

export const {
  previewProduct,
  changeStatus,
  previewEditProduct,
  delPreviewEditProduct,
  resetReducerProduct,
  clearProducts,
} = productSlice.actions;

export default productSlice.reducer;

import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const getAllCategory = createAsyncThunk(
  "category/getAllCategory",
  async (_, { rejectWithValue }) => {
    try {
      const response = await axios.get(
        "https://secondhand-serenades.herokuapp.com/api/v1/category/all"
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const getCategoryByName = createAsyncThunk(
  "category/getCategoryByName",
  async (payload, { rejectWithValue }, nameCategory) => {
    try {
      const response = await axios.post(
        `https://secondhand-serenades.herokuapp.com/api/v1/category/find_by_name?category_name=${nameCategory}`,
        payload
      );
      return response.data.data;
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

const initialState = {
  category: [],
  loading: false,
  status: "idle",
  isError: null,
};

export const categorySlice = createSlice({
  name: "category",
  initialState,
  reducers: {
    resetReducerCategory(state, action) {
      return initialState;
    },
  },
  extraReducers: {
    // Handle Get All Category
    [getAllCategory.pending]: (state) => {
      state.loading = true;
      state.category = null;
      state.status = "loading";
      state.isError = null;
    },
    [getAllCategory.fulfilled]: (state, action) => {
      state.category = action.payload;
      state.isError = null;
      state.status = "succeeded";
      state.loading = false;
    },
    [getAllCategory.rejected]: (state, action) => {
      state.loading = false;
      state.status = "failed";
      state.category = null;
      state.isError = action.payload;
    },
    // Handle Get All Category
  },
});

export const { resetReducerCategory } = categorySlice.actions;

export default categorySlice.reducer;

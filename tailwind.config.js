/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        bgColorMain: "#FFE9C9",
        bgColorSecond: "#E2D4F0",
        btnPrimary: "#7126B5",
        btnPrimaryHover: "#4E197C",
        btnDisable: "#8A8A8A",
        greenSuccess: "#73CA5C",
      },
      fontFamily: {
        body: ["Poppins"],
      },
    },
  },
  plugins: [require("tailwind-scrollbar-hide")],
};
